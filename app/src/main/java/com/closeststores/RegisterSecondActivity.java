package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.closeststores.Adapter.Selecttype_Adapter;
import com.closeststores.Adapter.Selecttype_Adapter_Sub;
import com.closeststores.Adapter.Selecttype_Adapter_Sub_Sub;
import com.closeststores.Model.select_type;
import com.closeststores.Model.select_type_sub;
import com.closeststores.Model.select_type_sub_sub;
import com.closeststores.Services.ApiConfig;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class RegisterSecondActivity extends AppCompatActivity {

    ImageView img_loc, img_loc_company, img_loc_seller;
    String status = "", message = "", str_category_type_name = "Select Type", data = "",
            seller_type = "", str_category_type_name_sub = "Select Sub", Str_category_type_name_sub_sub = "Select Sub Sub";
    LinearLayout l_buyer, l_seller, l_company;
    Spinner spin_business_seller, spin_business_company, spinner_seller_category_sub,
            spinner_seller_category_sub_sub, spin_company_sub, spin_company_subsub;
    ArrayList<select_type> data_selecttype = new ArrayList<>();
    JSONObject type, typeSub;
    JSONArray jsonArray, jsonArraySub, jsonArraySubSub;
    select_type sel_type;
    Selecttype_Adapter select_adapter;
    Button btn_next;
    select_type_sub sel_type_sub;
    ArrayList<select_type_sub> data_selecttype_sub = new ArrayList<>();
    Selecttype_Adapter_Sub select_adapter_sub;

    select_type_sub_sub sel_type_sub_sub;
    ArrayList<select_type_sub_sub> data_selecttype_sub_sub = new ArrayList<>();
    Selecttype_Adapter_Sub_Sub select_adapter_sub_sub;

    String str_select_type = "", first = "", last = "", user = "", email = "", phone = "", otp = "", image_path = "", password = "";
    TextInputEditText et_address, et_city, et_state, et_pincode, et_shop_name, et_reg_no,
            et_address_seller, et_city_seller, et_state_seller, et_pincode_seller,
            et_company_name, et_owner_name, et_owner_contact, et_cor_name, et_cor_contact,
            et_reg_no_company, et_address_company, et_city_company, et_state_company, et_pincode_company;
    CircleImageView img_profile_seller, img_profile_edit_seller, img_profile_company, img_profile_edit_company;
    int GALLERY_PICK = 101, CAMERA_PICK = 132;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    LinearLayout l_sub_category, l_company_sub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_second);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent i = getIntent();
        str_select_type = i.getStringExtra("select_type");
        first = i.getStringExtra("first");
        last = i.getStringExtra("last");
        user = i.getStringExtra("username");
        email = i.getStringExtra("email");
        phone = i.getStringExtra("phone");
        otp = i.getStringExtra("otp");
        password = i.getStringExtra("password");

        openRun();

        img_loc = (ImageView) findViewById(R.id.img_loc);
        img_loc_company = (ImageView) findViewById(R.id.img_loc_company);
        img_loc_seller = (ImageView) findViewById(R.id.img_loc_seller);
        l_buyer = (LinearLayout) findViewById(R.id.linear_buyer);
        l_seller = (LinearLayout) findViewById(R.id.linear_seller);
        l_company = (LinearLayout) findViewById(R.id.linear_company);
        spin_business_seller = (Spinner) findViewById(R.id.business_category);
        spin_business_company = (Spinner) findViewById(R.id.business_category_company);
        spinner_seller_category_sub = (Spinner) findViewById(R.id.business_category_sub);
        spinner_seller_category_sub_sub = (Spinner) findViewById(R.id.business_category_sub_sub);
        spin_company_sub = (Spinner) findViewById(R.id.business_category_company_sub);
        spin_company_subsub = (Spinner) findViewById(R.id.business_category_company_subsub);
        btn_next = (Button) findViewById(R.id.btn_next);
        l_sub_category = (LinearLayout) findViewById(R.id.l_sub_category);
        l_company_sub = (LinearLayout) findViewById(R.id.l_company_sub);

        et_address = (TextInputEditText) findViewById(R.id.address);
        et_city = (TextInputEditText) findViewById(R.id.city);
        et_state = (TextInputEditText) findViewById(R.id.state);
        et_pincode = (TextInputEditText) findViewById(R.id.pincode);

        et_shop_name = (TextInputEditText) findViewById(R.id.shopname);
        et_reg_no = (TextInputEditText) findViewById(R.id.reg_number);
        et_address_seller = (TextInputEditText) findViewById(R.id.address_seller);
        et_city_seller = (TextInputEditText) findViewById(R.id.cityseller);
        et_state_seller = (TextInputEditText) findViewById(R.id.stateseller);
        et_pincode_seller = (TextInputEditText) findViewById(R.id.pincodeseller);

        et_company_name = (TextInputEditText) findViewById(R.id.companyname);
        et_owner_name = (TextInputEditText) findViewById(R.id.owner_name);
        et_owner_contact = (TextInputEditText) findViewById(R.id.owner_contact);
        et_cor_name = (TextInputEditText) findViewById(R.id.cooridator_name);
        et_cor_contact = (TextInputEditText) findViewById(R.id.cooridator_contact);
        et_reg_no_company = (TextInputEditText) findViewById(R.id.reg_number_company);
        et_address_company = (TextInputEditText) findViewById(R.id.address_company);
        et_city_company = (TextInputEditText) findViewById(R.id.citycompany);
        et_state_company = (TextInputEditText) findViewById(R.id.statecompany);
        et_pincode_company = (TextInputEditText) findViewById(R.id.pincodecompany);

        et_owner_name.setText(first);
        et_owner_contact.setText(phone);

        img_profile_seller = (CircleImageView)findViewById(R.id.img_profile_edit_seller);
        img_profile_edit_seller = (CircleImageView)findViewById(R.id.img_profile_edit_icon_seller);
        img_profile_company = (CircleImageView)findViewById(R.id.img_company_profile);
        img_profile_edit_company = (CircleImageView)findViewById(R.id.img_company_profile_edit);

        if (str_select_type.equals("1")){
            l_buyer.setVisibility(View.VISIBLE);
            l_seller.setVisibility(View.GONE);
            l_company.setVisibility(View.GONE);
        }else if (str_select_type.equals("2")){
            new showselecttype().execute();
            l_seller.setVisibility(View.VISIBLE);
            l_buyer.setVisibility(View.GONE);
            l_company.setVisibility(View.GONE);
        }else if (str_select_type.equals("3")){
            new showselecttype().execute();
            l_seller.setVisibility(View.GONE);
            l_buyer.setVisibility(View.GONE);
            l_company.setVisibility(View.VISIBLE);
        }

        spin_business_seller.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype.get(position).getName().equals("Select Type")) {
                    str_category_type_name = "";
                    spinner_seller_category_sub.setVisibility(View.GONE);
                    l_sub_category.setVisibility(View.GONE);
                    spin_company_sub.setVisibility(View.GONE);
                    l_company_sub.setVisibility(View.GONE);
//                    spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                    spin_company_subsub.setVisibility(View.GONE);
//                    Toast.makeText(getApplicationContext(), getString(R.string.please_select_type), Toast.LENGTH_SHORT).show();
                } else {
                    str_category_type_name = data_selecttype.get(position).getId();
                    data_selecttype_sub.clear();
                    new showselecttypesub().execute();
                    spinner_seller_category_sub.setVisibility(View.VISIBLE);
                    l_sub_category.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_business_company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype.get(position).getName().equals("Select Type")) {
                    str_category_type_name = "";
                    spinner_seller_category_sub.setVisibility(View.GONE);
                    l_sub_category.setVisibility(View.GONE);
                    spin_company_sub.setVisibility(View.GONE);
                    l_company_sub.setVisibility(View.GONE);
//                    spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                    spin_company_subsub.setVisibility(View.GONE);
                } else {
                    str_category_type_name = data_selecttype.get(position).getId();
                    data_selecttype_sub.clear();
                    new showselecttypesub().execute();
                    spin_company_sub.setVisibility(View.VISIBLE);
                    l_company_sub.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_seller_category_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype_sub.get(position).getName().equals("Select Sub")) {
                    str_category_type_name_sub = "";
//                    spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                    spin_company_subsub.setVisibility(View.GONE);
                } else {
                    str_category_type_name_sub = data_selecttype_sub.get(position).getId();
                    data_selecttype_sub_sub.clear();
//                    new showselecttypesubsub().execute();
//                    spinner_seller_category_sub_sub.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_company_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype_sub.get(position).getName().equals("Select Sub")) {
                    str_category_type_name_sub = "";
//                    spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                    spin_company_subsub.setVisibility(View.GONE);
                } else {
                    str_category_type_name_sub = data_selecttype_sub.get(position).getId();
                    data_selecttype_sub_sub.clear();
//                    new showselecttypesubsub().execute();
//                    spin_company_subsub.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_seller_category_sub_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype_sub_sub.get(position).getName().equals("Select Sub Sub")) {
                    Str_category_type_name_sub_sub = "";
                } else {
                    Str_category_type_name_sub_sub = data_selecttype_sub_sub.get(position).getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_company_subsub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_selecttype_sub_sub.get(position).getName().equals("Select Sub Sub")) {
                    Str_category_type_name_sub_sub = "";
                } else {
                    Str_category_type_name_sub_sub = data_selecttype_sub_sub.get(position).getId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent location = new Intent(getApplicationContext(), AddAddressActivity.class);
                startActivity(location);
            }
        });

        img_loc_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent location = new Intent(getApplicationContext(), AddAddressActivity.class);
                startActivity(location);
            }
        });

        img_loc_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent location = new Intent(getApplicationContext(), AddAddressActivity.class);
                startActivity(location);
            }
        });

        img_profile_edit_seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        img_profile_edit_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_select_type.equals("1")){
                    if (et_address.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_address), Toast.LENGTH_LONG).show();
                    }else if (et_city.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_city), Toast.LENGTH_LONG).show();
                    }else if (et_state.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_state), Toast.LENGTH_LONG).show();
                    }else if (et_pincode.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_pincode), Toast.LENGTH_LONG).show();
                    } else if (et_pincode.getText().toString().trim().length() < 6){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.minimum_pincode), Toast.LENGTH_LONG).show();
                    }
                    else{
                        Intent register = new Intent(getApplicationContext(), RegisterThirdActivity.class);
                        register.putExtra("select_type", str_select_type);
                        register.putExtra("address", et_address.getText().toString().trim());
                        register.putExtra("city", et_city.getText().toString().trim());
                        register.putExtra("state", et_state.getText().toString().trim());
                        register.putExtra("pincode", et_pincode.getText().toString().trim());
                        register.putExtra("first", first);
                        register.putExtra("last", last);
                        register.putExtra("username", user);
                        register.putExtra("email", email);
                        register.putExtra("phone", phone);
                        register.putExtra("otp", otp);
                        register.putExtra("password", password);
                        register.putExtra("owner_name", et_owner_name.getText().toString().trim());
                        register.putExtra("owner_contact", et_owner_contact.getText().toString().trim());
                        register.putExtra("cor_name", et_cor_name.getText().toString().trim());
                        register.putExtra("cor_contact", et_cor_contact.getText().toString().trim());
                        register.putExtra("shop_name", et_shop_name.getText().toString().trim());
                        register.putExtra("reg_no", et_reg_no.getText().toString().trim());
                        register.putExtra("image", image_path);
                        register.putExtra("category", str_category_type_name);
                        register.putExtra("sub_category", str_category_type_name_sub);
                        register.putExtra("sub_sub_category", "25");  //Str_category_type_name_sub_sub
                        startActivity(register);
                    }
                }
                else if (str_select_type.equals("2")){
                    if (et_shop_name.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_owner_shope_name), Toast.LENGTH_LONG).show();
                    }else if (et_reg_no.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.shop_register_id), Toast.LENGTH_LONG).show();
                    }else if (et_address_seller.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_address), Toast.LENGTH_LONG).show();
                    }else if (et_city_seller.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_city), Toast.LENGTH_LONG).show();
                    }else if (et_state_seller.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_state), Toast.LENGTH_LONG).show();
                    }else if (et_pincode_seller.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_pincode), Toast.LENGTH_LONG).show();
                    }else if (et_pincode_seller.getText().toString().trim().length() < 6){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.minimum_pincode), Toast.LENGTH_LONG).show();
                    }else{
                        Intent register = new Intent(getApplicationContext(), RegisterThirdActivity.class);
                        register.putExtra("select_type", str_select_type);
                        register.putExtra("shop_name", et_shop_name.getText().toString().trim());
                        register.putExtra("reg_no", et_reg_no.getText().toString().trim());
                        register.putExtra("address", et_address_seller.getText().toString().trim());
                        register.putExtra("city", et_city_seller.getText().toString().trim());
                        register.putExtra("state", et_state_seller.getText().toString().trim());
                        register.putExtra("pincode", et_pincode_seller.getText().toString().trim());
                        register.putExtra("first", first);
                        register.putExtra("last", last);
                        register.putExtra("username", user);
                        register.putExtra("email", email);
                        register.putExtra("phone", phone);
                        register.putExtra("otp", otp);
                        register.putExtra("password", password);
                        register.putExtra("owner_name", et_owner_name.getText().toString().trim());
                        register.putExtra("owner_contact", et_owner_contact.getText().toString().trim());
                        register.putExtra("cor_name", et_cor_name.getText().toString().trim());
                        register.putExtra("cor_contact", et_cor_contact.getText().toString().trim());
                        register.putExtra("image", image_path);
                        register.putExtra("category", str_category_type_name);
                        register.putExtra("sub_category", str_category_type_name_sub);
                        register.putExtra("sub_sub_category", Str_category_type_name_sub_sub);
                        startActivity(register);
                    }
                }
                else if (str_select_type.equals("3")){
                    if (et_company_name.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_company_name), Toast.LENGTH_LONG).show();
                    }else if (et_owner_name.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_owner_name), Toast.LENGTH_LONG).show();
                    }else if (et_owner_contact.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_mobile), Toast.LENGTH_LONG).show();
                    }else if (et_owner_contact.getText().toString().trim().length() < 10){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.minimum_no), Toast.LENGTH_LONG).show();
                    }
                    else if (et_cor_name.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_coodinator_name), Toast.LENGTH_LONG).show();
                    }else if (et_cor_contact.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_coodinator_contact), Toast.LENGTH_LONG).show();
                    }
                    else if (et_cor_contact.getText().toString().trim().length() < 10){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.minimum_no), Toast.LENGTH_LONG).show();
                    }else if (et_reg_no_company.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.shop_register_id), Toast.LENGTH_LONG).show();
                    }else if (et_address_company.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_address), Toast.LENGTH_LONG).show();
                    }else if (et_city_company.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_city), Toast.LENGTH_LONG).show();
                    }else if (et_state_company.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_state), Toast.LENGTH_LONG).show();
                    }else if (et_pincode_company.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.please_enter_pincode), Toast.LENGTH_LONG).show();
                    }else if (et_pincode_company.getText().toString().trim().length() < 6){
                        Toast.makeText(RegisterSecondActivity.this, getString(R.string.minimum_pincode), Toast.LENGTH_LONG).show();
                    }else{
                        Intent register = new Intent(getApplicationContext(), RegisterThirdActivity.class);
                        register.putExtra("select_type", str_select_type);
                        register.putExtra("shop_name", et_company_name.getText().toString().trim());
                        register.putExtra("reg_no", et_reg_no_company.getText().toString().trim());
                        register.putExtra("address", et_address_company.getText().toString().trim());
                        register.putExtra("city", et_city_company.getText().toString().trim());
                        register.putExtra("state", et_state_company.getText().toString().trim());
                        register.putExtra("pincode", et_pincode_company.getText().toString().trim());
                        register.putExtra("first", first);
                        register.putExtra("last", last);
                        register.putExtra("username", user);
                        register.putExtra("email", email);
                        register.putExtra("phone", phone);
                        register.putExtra("otp", otp);
                        register.putExtra("owner_name", et_owner_name.getText().toString().trim());
                        register.putExtra("owner_contact", et_owner_contact.getText().toString().trim());
                        register.putExtra("cor_name", et_cor_name.getText().toString().trim());
                        register.putExtra("cor_contact", et_cor_contact.getText().toString().trim());
                        register.putExtra("password", password);
                        register.putExtra("image", image_path);
                        register.putExtra("category", str_category_type_name);
                        register.putExtra("sub_category", str_category_type_name_sub);
                        register.putExtra("sub_sub_category", Str_category_type_name_sub_sub);
                        startActivity(register);
                    }
                }

            }
        });
    }

    private void openRun() {
        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyContactWrapper();
        } else {
        }
    }

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Storage");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    openRun();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showPictureDialog() {
        final String[] option = getResources().getStringArray(R.array.select_action);
        final AlertDialog.Builder pictureDialog = new AlertDialog.Builder(RegisterSecondActivity.this);
        pictureDialog.setTitle(getResources().getString(R.string.select_action_text));

        pictureDialog.setItems(option, new DialogInterface.OnClickListener() {
            @SuppressLint("IntentReset")
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (option[i].equals(getResources().getString(R.string.image_select_gallery))) {
                    Intent GALLERY = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    GALLERY.setType("image/*");
                    startActivityForResult(Intent.createChooser(GALLERY, "Select Image"), GALLERY_PICK);
                } else if (option[i].equals(getResources().getString(R.string.image_select_camera))) {
                    Intent intent_camera = new Intent("android.media.action.IMAGE_CAPTURE");
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                    intent_camera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent_camera, CAMERA_PICK);
                } else if (option[i].equals(getResources().getString(R.string.image_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        pictureDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_PICK) {
                Uri uri = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
                Cursor cursor = RegisterSecondActivity.this.getContentResolver().query(uri, FILE, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(FILE[0]);
                image_path = cursor.getString(columnIndex);
                cursor.close();

                if (str_select_type.equals("2")){
                    img_profile_seller.setImageURI(uri);
                }else if (str_select_type.equals("3")){
                    img_profile_company.setImageURI(uri);
                }


            } else if (requestCode == CAMERA_PICK) {
                try {
                    image_path = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                    if (str_select_type.equals("2")){
                        img_profile_seller.setImageBitmap(BitmapFactory.decodeFile(image_path));
                    }else if (str_select_type.equals("3")){
                        img_profile_company.setImageBitmap(BitmapFactory.decodeFile(image_path));
                    }

//                    img_profile_company.setImageBitmap(BitmapFactory.decodeFile(image_path));
                } catch (Exception e) {

                }
            }
        }
    }

    // Login Api Call
    @SuppressLint("StaticFieldLeak")
    public class showselecttype extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterSecondActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.MAIN_CATEGORY);
                JSONObject postDataParams = new JSONObject();

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            LoginResponse(result);
        }
    }

    private void LoginResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {

                    jsonArray = jsobjectcategory.getJSONArray("message");

                    sel_type = new select_type();
                    sel_type.setId("");
                    sel_type.setName("Select Type");
                    data_selecttype.add(sel_type);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                        sel_type = new select_type();

                        String Use_Id = jsonObject1.getString("category_id");
                        String Use_Name = jsonObject1.getString("name");

                        sel_type.setId(Use_Id);
                        sel_type.setName(Use_Name);
                        data_selecttype.add(sel_type);
                    }

                    select_adapter = new Selecttype_Adapter(RegisterSecondActivity.this, data_selecttype);
                    if (str_select_type.equals("2")){
                        spin_business_seller.setAdapter(select_adapter);
                    }else if (str_select_type.equals("3")){
                        spin_business_company.setAdapter(select_adapter);
                    }

                }else {
                    Toast.makeText(RegisterSecondActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class showselecttypesub extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterSecondActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.MAIN_CATEGORY_SUB);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("parent_id", str_category_type_name);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            SubResponse(result);
        }
    }

    private void SubResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {

                    try {
                        jsonArraySub = jsobjectcategory.getJSONArray("message");

                        if (jsonArraySub.length() > 0){
                            sel_type_sub = new select_type_sub();
                            sel_type_sub.setId("");
                            sel_type_sub.setName("Select Sub");
                            sel_type_sub.setParent_id("");
                            data_selecttype_sub.add(sel_type_sub);

                            for (int i = 0; i < jsonArraySub.length(); i++) {
                                JSONObject jsonObject1 = jsonArraySub.getJSONObject(i);

                                sel_type_sub = new select_type_sub();

                                String Use_Id = jsonObject1.getString("category_id");
                                String Use_Name = jsonObject1.getString("name");
                                String Parent_ID = jsonObject1.getString("parent_id");

                                sel_type_sub.setId(Use_Id);
                                sel_type_sub.setName(Use_Name);
                                sel_type_sub.setParent_id(Parent_ID);
                                data_selecttype_sub.add(sel_type_sub);
                            }

                            select_adapter_sub = new Selecttype_Adapter_Sub(RegisterSecondActivity.this, data_selecttype_sub);
                            if (str_select_type.equals("2")){
                                spinner_seller_category_sub.setVisibility(View.VISIBLE);
                                l_sub_category.setVisibility(View.VISIBLE);
                                spinner_seller_category_sub.setAdapter(select_adapter_sub);
                            }else if (str_select_type.equals("3")){
                                spin_company_sub.setVisibility(View.VISIBLE);
                                l_company_sub.setVisibility(View.VISIBLE);
                                spin_company_sub.setAdapter(select_adapter_sub);
                            }
                        }else{
                            spinner_seller_category_sub.setVisibility(View.GONE);
                            l_sub_category.setVisibility(View.GONE);
                            spin_company_sub.setVisibility(View.GONE);
                            l_company_sub.setVisibility(View.GONE);
//                            spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                            spin_company_subsub.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(RegisterSecondActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class showselecttypesubsub extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterSecondActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.MAIN_CATEGORY_SUB_SUB);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("parent_id", str_category_type_name);
                postDataParams.put("sub_id", str_category_type_name_sub);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            SubsubResponse(result);
        }
    }

    private void SubsubResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");
                String data = jsobjectcategory.getString("data");

                if (status.equals("1")) {

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        jsonArraySubSub = jsonObject.getJSONArray("subsubcategory");

                        if (jsonArraySub.length() > 0){
                            sel_type_sub_sub = new select_type_sub_sub();
                            sel_type_sub_sub.setId("");
                            sel_type_sub_sub.setName("Select Sub Sub");
                            sel_type_sub_sub.setParent_id("");
                            data_selecttype_sub_sub.add(sel_type_sub_sub);

                            for (int i = 0; i < jsonArraySubSub.length(); i++) {
                                JSONObject jsonObject1 = jsonArraySubSub.getJSONObject(i);

                                sel_type_sub_sub = new select_type_sub_sub();

                                String Use_Id = jsonObject1.getString("id");
                                String Use_Name = jsonObject1.getString("slug");
                                String Parent_ID = jsonObject1.getString("parent_id");

                                sel_type_sub_sub.setId(Use_Id);
                                sel_type_sub_sub.setName(Use_Name);
                                sel_type_sub_sub.setParent_id(Parent_ID);
                                data_selecttype_sub_sub.add(sel_type_sub_sub);
                            }

                            select_adapter_sub_sub = new Selecttype_Adapter_Sub_Sub(RegisterSecondActivity.this, data_selecttype_sub_sub);
                            if (str_select_type.equals("2")){
//                                spinner_seller_category_sub_sub.setVisibility(View.VISIBLE);
//                                spinner_seller_category_sub_sub.setAdapter(select_adapter_sub_sub);
                            }else if (str_select_type.equals("3")){
//                                spin_company_subsub.setVisibility(View.VISIBLE);
//                                spin_company_subsub.setAdapter(select_adapter_sub_sub);
                            }
                        }else{
//                            spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                            spin_company_subsub.setVisibility(View.GONE);
//                            spinner_seller_category_sub_sub.setVisibility(View.GONE);
//                            spin_company_subsub.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(RegisterSecondActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}