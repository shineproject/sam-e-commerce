package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.closeststores.Adapter.MyOrderAdapter;
import com.closeststores.Adapter.OrderCartAdapter;
import com.closeststores.Helper.RecyclerItemClickListener;

import java.util.ArrayList;

public class MyOrderActivity extends AppCompatActivity {

    RecyclerView rv_my_order;
    MyOrderAdapter myOrderAdapter;
    ArrayList<String> myOrder = new ArrayList<>();

    TextView txt_name;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);

        rv_my_order = (RecyclerView) findViewById(R.id.rv_my_order);
        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("My Order");

        myOrder.add("501876");
        myOrder.add("501877");
        myOrder.add("501878");
        myOrder.add("501879");
        myOrder.add("501880");

        myOrderAdapter = new MyOrderAdapter(getApplicationContext(), myOrder);
        rv_my_order.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayout.VERTICAL, false));
        rv_my_order.setAdapter(myOrderAdapter);

        rv_my_order.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent order = new Intent(getApplicationContext(), OrderDetailActivity.class);
                        startActivity(order);
                    }
                })
        );
    }
}