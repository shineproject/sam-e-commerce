package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Order_Payment_Detail_Activity extends AppCompatActivity {

    TextView txt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__payment__detail_);

        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Order Payment Detail");
    }
}