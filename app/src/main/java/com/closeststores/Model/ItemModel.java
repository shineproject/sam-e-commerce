package com.closeststores.Model;

public class ItemModel {
    String name;

    public ItemModel() {
    }

    public ItemModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
