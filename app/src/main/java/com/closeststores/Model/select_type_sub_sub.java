package com.closeststores.Model;

public class select_type_sub_sub {
    String id;
    String name;
    String parent_id;

    public select_type_sub_sub() {
    }

    public select_type_sub_sub(String id, String name, String parent_id){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }
}
