package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.closeststores.Adapter.Selectsecret_Adapter;
import com.closeststores.Model.select_secret;
import com.closeststores.Services.ApiConfig;
import com.closeststores.Utils.ConnectivityDetector;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class ForgotPasswordActivity extends AppCompatActivity {

    Spinner spin_secret_qus;
    EditText et_secret;
    ArrayList<select_secret> data_secret = new ArrayList<>();
    JSONArray jsonArraySecret;
    select_secret sel_secret;
    Selectsecret_Adapter select_secret_adapter;
    String str_secret = "";
    TextInputEditText et_username, et_mobile;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        spin_secret_qus = (Spinner) findViewById(R.id.spin_secret_question);
        et_secret = (EditText) findViewById(R.id.et_secret_qus);
        et_username = findViewById(R.id.username);
        et_mobile = findViewById(R.id.email);
        btn_submit = findViewById(R.id.btn_submit);

        new showselectqus().execute();

        spin_secret_qus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_secret.get(position).getName().equals("Select Secret Question")) {
                    str_secret = "Select Secret Question";
                    et_secret.setText("");
                    et_secret.setVisibility(View.GONE);
                } else {
                    str_secret = data_secret.get(position).getId();
                    et_secret.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_username.getText().toString().trim().equals("")) {
                    Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_name), Toast.LENGTH_LONG).show();
                } else if (et_mobile.getText().toString().trim().equals("")) {
                    Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_mobile), Toast.LENGTH_LONG).show();
                }else if (!et_mobile.getText().toString().trim().equals("")) {
                    if (TextUtils.isDigitsOnly(et_mobile.getText())){
                         if (et_secret.getText().toString().trim().equals("")){
                            Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                        }else {
                            if (ConnectivityDetector.isConnectingToInternet(ForgotPasswordActivity.this)) {
                                new ForgotPass().execute();
                            } else {
                                Toast.makeText(ForgotPasswordActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                            }
                        }
                    }else{
                        if (!Patterns.EMAIL_ADDRESS.matcher(et_mobile.getText().toString()).matches()) {
                            Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_valid), Toast.LENGTH_LONG).show();
                        }else {
                             if (et_secret.getText().toString().trim().equals("")){
                                Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                            }else {
                                if (ConnectivityDetector.isConnectingToInternet(ForgotPasswordActivity.this)) {
                                    new ForgotPass().execute();
                                } else {
                                    Toast.makeText(ForgotPasswordActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                    }
                }
                else if (et_secret.getText().toString().trim().equals("")){
                    Toast.makeText(ForgotPasswordActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                }else {
                    if (ConnectivityDetector.isConnectingToInternet(ForgotPasswordActivity.this)) {
                        new ForgotPass().execute();
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public class showselectqus extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(ForgotPasswordActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.QUESTION_LIST);
                JSONObject postDataParams = new JSONObject();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            QusResponse(result);
        }
    }

    private void QusResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");
                String data = jsobjectcategory.getString("data");

                if (status.equals("1")) {

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        jsonArraySecret = jsonObject.getJSONArray("secretquestionlist");

                        if (jsonArraySecret.length() > 0){
                            sel_secret = new select_secret();
                            sel_secret.setId("");
                            sel_secret.setName("Select Secret Question");
                            data_secret.add(sel_secret);

                            for (int i = 0; i < jsonArraySecret.length(); i++) {
                                JSONObject jsonObject1 = jsonArraySecret.getJSONObject(i);

                                sel_secret = new select_secret();

                                String Use_Id = jsonObject1.getString("id");
                                String Use_Name = jsonObject1.getString("name");

                                sel_secret.setId(Use_Id);
                                sel_secret.setName(Use_Name);
                                data_secret.add(sel_secret);
                            }

                            select_secret_adapter = new Selectsecret_Adapter(ForgotPasswordActivity.this, data_secret);
                            spin_secret_qus.setAdapter(select_secret_adapter);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    // Login Api Call
    @SuppressLint("StaticFieldLeak")
    public class ForgotPass extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(ForgotPasswordActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.FORGOT_PASSWORD);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("username", et_username.getText().toString().trim());
                postDataParams.put("email", et_mobile.getText().toString().trim());
                postDataParams.put("question_id", str_secret);
                postDataParams.put("question_answer", et_secret.getText().toString().trim());

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            ForgotResponse(result);
        }
    }

    private void ForgotResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {
                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_LONG).show();
                    Intent profile = new Intent(getApplicationContext(), MainActivity.class);
                    profile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(profile);
                    finish();
                }else {
                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}