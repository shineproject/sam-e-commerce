package com.closeststores;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.closeststores.Fragment.FeedbackFragment;
import com.closeststores.Fragment.HistoryFragment;
import com.closeststores.Fragment.HomeFragment;
import com.closeststores.Fragment.OrderFragment;
import com.closeststores.Model.MenuModel;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class DashBoardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    String[] name;
    DrawerLayout drawer;
    ImageView img_menu;
    ArrayList<MenuModel> data;
    MenuAdapter menuAdapter;
    RecyclerView rv_drawer_menu_list;
    boolean isOpen = true;
    MeowBottomNavigation bottomNavigation;
    int[] image = {R.drawable.home, R.drawable.order, R.drawable.history, R.drawable.feedback,
            R.drawable.address, R.drawable.complain, R.drawable.profile};
    TextView txt_home, txt_order, txt_history, txt_feedback, text_username, text_email;
    String user_name = "", user_email = "";

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        SharedPreferences splogin = DashBoardActivity.this.getSharedPreferences("login", 0);
        user_name = splogin.getString("user_name", "");
        user_email = splogin.getString("email", "");

        name = getResources().getStringArray(R.array.menu);

        img_menu = (ImageView)findViewById(R.id.img_menu);
        rv_drawer_menu_list = (RecyclerView) findViewById(R.id.rv_parent_drawer_menu_list);
        bottomNavigation = findViewById(R.id.bottomNavigation);
        txt_home = findViewById(R.id.txt_home);
        txt_order = findViewById(R.id.txt_order);
        txt_history = findViewById(R.id.txt_history);
        txt_feedback = findViewById(R.id.txt_feedback);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        text_username = findViewById(R.id.txt_username);
        text_email = findViewById(R.id.txt_email);

        text_username.setText(user_name);
        text_email.setText(user_email);

        img_menu.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                boolean mSlideState = false;
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {
                    drawer.openDrawer(Gravity.START);
                }
            }
        });

        data = new ArrayList<>();
        for (int i = 0; i < image.length; i++) {
            MenuModel menuModel = new MenuModel();
            menuModel.setName(name[i]);
            menuModel.setImage(image[i]);
            data.add(menuModel);
        }

        menuAdapter = new MenuAdapter(DashBoardActivity.this, data);
        menuAdapter.notifyDataSetChanged();
        rv_drawer_menu_list.setLayoutManager(new LinearLayoutManager(DashBoardActivity.this, LinearLayout.VERTICAL, false));
        rv_drawer_menu_list.setAdapter(menuAdapter);

        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.home_bottom));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.order_bottom));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.history_bottom));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.feedback_bottom));

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                Fragment selectedFragment = null;
                String name;
                switch (item.getId()) {
                    case 1:
                        name = "HOME";
                        selectedFragment = HomeFragment.newInstance();
                        txt_home.setTextColor(getResources().getColor(R.color.colorPrimary));
                        txt_order.setTextColor(getResources().getColor(R.color.black));
                        txt_history.setTextColor(getResources().getColor(R.color.black));
                        txt_feedback.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case 2:
                        name = "ORDER";
                        selectedFragment = OrderFragment.newInstance();
                        txt_home.setTextColor(getResources().getColor(R.color.black));
                        txt_order.setTextColor(getResources().getColor(R.color.colorPrimary));
                        txt_history.setTextColor(getResources().getColor(R.color.black));
                        txt_feedback.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case 3:
                        name = "HISTORY";
                        selectedFragment = HistoryFragment.newInstance();
                        txt_home.setTextColor(getResources().getColor(R.color.black));
                        txt_order.setTextColor(getResources().getColor(R.color.black));
                        txt_history.setTextColor(getResources().getColor(R.color.colorPrimary));
                        txt_feedback.setTextColor(getResources().getColor(R.color.black));
                        break;
                    case 4:
                        name = "FEEDBACK";
                        selectedFragment = FeedbackFragment.newInstance();
                        txt_home.setTextColor(getResources().getColor(R.color.black));
                        txt_order.setTextColor(getResources().getColor(R.color.black));
                        txt_history.setTextColor(getResources().getColor(R.color.black));
                        txt_feedback.setTextColor(getResources().getColor(R.color.colorPrimary));
                        break;

                    default:
                        name = "";


                }

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }
        });

        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
        transaction.commit();

        txt_home.setTextColor(getResources().getColor(R.color.colorPrimary));
        txt_order.setTextColor(getResources().getColor(R.color.black));
        txt_history.setTextColor(getResources().getColor(R.color.black));
        txt_feedback.setTextColor(getResources().getColor(R.color.black));

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
            }
        });

        bottomNavigation.show(1,true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
        private Context context;
        private ArrayList<MenuModel> data;

        public MenuAdapter(Context context, ArrayList<MenuModel> data) {
            this.context = context;
            this.data = data;
        }

        @Override
        public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.menu_list, parent, false);
            return new MenuAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MenuAdapter.ViewHolder holder, final int position) {

            final MenuModel menuModel = data.get(position);
            holder.txt_menu.setText("" + menuModel.getName());
            Glide.with(context).load(menuModel.getImage()).placeholder(R.mipmap.ic_launcher_round).
                    error(R.mipmap.ic_launcher_round).into(holder.img_menu);

            holder.txt_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isOpen = false;
                    String name = menuModel.getName();
                    if (name.equals(getResources().getString(R.string.menu_home))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }else if (name.equals(getResources().getString(R.string.menu_order))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);

                        Intent profile = new Intent(getApplicationContext(), OrderCartActivity.class);
                        startActivity(profile);
                    }else if (name.equals(getResources().getString(R.string.menu_history))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }else if (name.equals(getResources().getString(R.string.menu_feedback))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }else if (name.equals(getResources().getString(R.string.menu_address))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }else if (name.equals(getResources().getString(R.string.menu_complain))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }else if (name.equals(getResources().getString(R.string.menu_profile))) {
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);

                        Intent profile = new Intent(getApplicationContext(), ProfileActivity.class);
                        startActivity(profile);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img_menu;
            TextView txt_menu;
            View view;

            public ViewHolder(View itemView) {
                super(itemView);
                img_menu = (ImageView) itemView.findViewById(R.id.img_menu);
                txt_menu = (TextView) itemView.findViewById(R.id.txt_menu);
                view = (View) itemView.findViewById(R.id.view);
            }
        }
    }
}