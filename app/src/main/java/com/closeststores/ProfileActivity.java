package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    TextView txt_name, txt_payment_method, txt_personal_info, txt_logout, txt_wishlist, txt_address, txt_order, txt_UserName;
    String id = "", vendor_type = "", user_name = "";
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        SharedPreferences splogin = ProfileActivity.this.getSharedPreferences("login", 0);
        id = splogin.getString("customer_id", "");
        vendor_type = splogin.getString("vendor_type", "");
        user_name = splogin.getString("user_name", "");

        txt_name = (TextView) findViewById(R.id.txtTitle);
        txt_payment_method = (TextView) findViewById(R.id.txtPaymentMethod);
        txt_personal_info = (TextView) findViewById(R.id.txtPersonalInfo);
        txt_logout = (TextView) findViewById(R.id.txtLogout);
        txt_wishlist = (TextView) findViewById(R.id.txtWishList);
        txt_address = (TextView) findViewById(R.id.txtAddressBook);
        txt_order = (TextView) findViewById(R.id.txtMyOrder);
        txt_UserName = (TextView) findViewById(R.id.txtUserName);
        img_back = (ImageView) findViewById(R.id.imgBack);

        txt_name.setText("Profile");
        txt_UserName.setText(user_name);

        if (vendor_type.equals("1")){
            txt_address.setVisibility(View.VISIBLE);
            txt_wishlist.setVisibility(View.VISIBLE);
            txt_order.setVisibility(View.VISIBLE);
            txt_payment_method.setVisibility(View.VISIBLE);
        }else if (vendor_type.equals("2")){
            txt_address.setVisibility(View.GONE);
            txt_wishlist.setVisibility(View.GONE);
            txt_order.setVisibility(View.GONE);
            txt_payment_method.setVisibility(View.GONE);
        }else if (vendor_type.equals("3")){
            txt_address.setVisibility(View.GONE);
            txt_wishlist.setVisibility(View.GONE);
            txt_order.setVisibility(View.GONE);
            txt_payment_method.setVisibility(View.GONE);
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txt_payment_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent profile = new Intent(getApplicationContext(), OrderPaymentActivity.class);
//                startActivity(profile);
            }
        });

        txt_personal_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent profile = new Intent(getApplicationContext(), PersonalInfoActivity.class);
//                startActivity(profile);
            }
        });

        txt_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent profile = new Intent(getApplicationContext(), WishListActivity.class);
//                startActivity(profile);
            }
        });

        txt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent profile = new Intent(getApplicationContext(), AddressListActivity.class);
//                startActivity(profile);
            }
        });

        txt_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent profile = new Intent(getApplicationContext(), MyOrderActivity.class);
//                startActivity(profile);
            }
        });

        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences spuser = ProfileActivity.this.getSharedPreferences("login", 0);
                SharedPreferences.Editor Eduser = spuser.edit();
                Eduser.putString("customer_group_id", null);
                Eduser.putString("store_id", null);
                Eduser.putString("customer_id", null);
                Eduser.putString("name", null);
                Eduser.putString("user_name", null);
                Eduser.putString("vendor_type", null);
                Eduser.putString("vendor_image", null);
                Eduser.putString("email",null);
                Eduser.putString("telephone",null);
                Eduser.apply();

                Intent profile = new Intent(getApplicationContext(), MainActivity.class);
                profile.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(profile);
                finish();
            }
        });
    }
}