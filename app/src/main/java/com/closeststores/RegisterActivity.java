package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.closeststores.Adapter.Vendortype_Adapter;
import com.closeststores.Model.vendor_type;
import com.closeststores.Services.ApiConfig;
import com.closeststores.Utils.ConnectivityDetector;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class RegisterActivity extends AppCompatActivity {

    Button btn_register, btn_otp;
    TextInputEditText et_first, et_last, et_username, et_email, et_phone, et_otp, et_password, et_confirm_password;
    Spinner spin_select_type;
    String str_select_type = "", str_otp;
    ImageView img_send_otp;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    ArrayList<vendor_type> data_vendortype = new ArrayList<>();
    Vendortype_Adapter select_adapter;
    JSONArray jsonArray;
    vendor_type sel_vendor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn_register = findViewById(R.id.btn_next);
        et_first = findViewById(R.id.firstname);
        et_last = findViewById(R.id.lastname);
        et_username = findViewById(R.id.username);
        et_email = findViewById(R.id.email);
        et_phone = findViewById(R.id.contact);
        et_otp = findViewById(R.id.otp);
        et_password = findViewById(R.id.password);
        et_confirm_password = findViewById(R.id.confirm_password);
        spin_select_type = findViewById(R.id.spinner_select_type);
        img_send_otp = findViewById(R.id.img_otp_send);
        btn_otp = findViewById(R.id.btn_otp);

        openRun();

        new showvendertype().execute();

        spin_select_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                str_select_type = data_vendortype.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_first.getText().toString().trim().equals("")) {
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_firstname), Toast.LENGTH_LONG).show();
                } else if (et_last.getText().toString().trim().equals("")) {
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_lastname), Toast.LENGTH_LONG).show();
                }else if (et_username.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_name), Toast.LENGTH_LONG).show();
                }else if (et_email.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_email), Toast.LENGTH_LONG).show();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.getText().toString()).matches()){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_valid_email), Toast.LENGTH_LONG).show();
                }else if (et_phone.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_phoneno), Toast.LENGTH_LONG).show();
                }else if (et_phone.getText().toString().trim().length() < 10){
                    Toast.makeText(RegisterActivity.this, getString(R.string.minimum_no), Toast.LENGTH_LONG).show();
                }else if (et_otp.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_otp), Toast.LENGTH_LONG).show();
                }else if (!et_otp.getText().toString().trim().equals(str_otp)){
                    Toast.makeText(RegisterActivity.this, getString(R.string.otp_not_match), Toast.LENGTH_LONG).show();
                } else if (et_password.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_password), Toast.LENGTH_LONG).show();
                }else if (et_confirm_password.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_confirm_password), Toast.LENGTH_LONG).show();
                } else if (!et_password.getText().toString().trim().equals(et_confirm_password.getText().toString().trim())){
                    Toast.makeText(RegisterActivity.this, getString(R.string.password_not_match), Toast.LENGTH_LONG).show();
                }else if (str_select_type.equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_select_category), Toast.LENGTH_LONG).show();
                }
                else {
                    if (str_select_type.equals("2")){
                        Intent register = new Intent(getApplicationContext(), RegisterSecondActivity.class);
                        register.putExtra("select_type", str_select_type);
                        register.putExtra("first", et_first.getText().toString().trim());
                        register.putExtra("last", et_last.getText().toString().trim());
                        register.putExtra("username", et_username.getText().toString().trim());
                        register.putExtra("email", et_email.getText().toString().trim());
                        register.putExtra("phone", et_phone.getText().toString().trim());
                        register.putExtra("otp", et_otp.getText().toString().trim());
                        register.putExtra("password", et_password.getText().toString().trim());
                        startActivity(register);
                    }else {
                        Toast.makeText(RegisterActivity.this, "Please select only seller", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        img_send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (et_phone.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_phoneno), Toast.LENGTH_LONG).show();
                }else if (et_phone.getText().toString().trim().length() < 10){
                   Toast.makeText(RegisterActivity.this, getString(R.string.minimum_no), Toast.LENGTH_LONG).show();
               }else {
                   if (ConnectivityDetector.isConnectingToInternet(RegisterActivity.this)) {
                       str_otp = "";
                       if (str_select_type.equals("2")){
                           new sendotp().execute();
                       }else {
                           Toast.makeText(RegisterActivity.this, "Please select only seller", Toast.LENGTH_LONG).show();
                       }
                   } else {
                       Toast.makeText(RegisterActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                   }
               }

            }
        });

        btn_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_phone.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, getString(R.string.please_enter_phoneno), Toast.LENGTH_LONG).show();
                }else if (et_phone.getText().toString().trim().length() < 10){
                    Toast.makeText(RegisterActivity.this, getString(R.string.minimum_no), Toast.LENGTH_LONG).show();
                }else {
                    if (ConnectivityDetector.isConnectingToInternet(RegisterActivity.this)) {
                        str_otp = "";
                        if (str_select_type.equals("2")){
                            new sendotp().execute();
                        }else {
                            Toast.makeText(RegisterActivity.this, "Please select only seller", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public class showvendertype extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.VENDOR_TYPE);
                JSONObject postDataParams = new JSONObject();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            VendorResponse(result);
        }
    }

    private void VendorResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {

                    try {
                        jsonArray = jsobjectcategory.getJSONArray("data");

                        if (jsonArray.length() > 0){

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                sel_vendor = new vendor_type();

                                String Use_Id = jsonObject1.getString("id");
                                String Use_Name = jsonObject1.getString("vendor_typename");

                                sel_vendor.setId(Use_Id);
                                sel_vendor.setName(Use_Name);
                                data_vendortype.add(sel_vendor);
                            }

                            select_adapter = new Vendortype_Adapter(RegisterActivity.this, data_vendortype);
                            spin_select_type.setAdapter(select_adapter);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void openRun() {
        if (Build.VERSION.SDK_INT >= 23) {
            insertDummyContactWrapper();
        } else {
        }
    }

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write Storage");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Read Storage");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    openRun();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // Login Api Call
    @SuppressLint("StaticFieldLeak")
    public class sendotp extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.SEND_OTP);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("country_code", "+91");
                postDataParams.put("phone_number", et_phone.getText().toString().trim());
                postDataParams.put("vendor_type", str_select_type);

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            LoginResponse(result);
        }
    }

    private void LoginResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");
                String otp = jsobjectcategory.getString("otp");

                if (status.equals("1")) {

                    String Otp = jsobjectcategory.getString("otp");
                    str_otp = Otp;
                    Toast.makeText(RegisterActivity.this, Otp, Toast.LENGTH_LONG).show();
                    btn_register.setClickable(true);
                    btn_register.setEnabled(true);
                    btn_register.setBackground(getResources().getDrawable(R.drawable.login_bg_white));

                }else {
                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}