package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class AddRequestActivity extends AppCompatActivity {

    TextView txt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request);

        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Add Request");
    }
}