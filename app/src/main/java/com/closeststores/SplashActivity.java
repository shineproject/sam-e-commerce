package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences splogin = SplashActivity.this.getSharedPreferences("login", 0);
        final String id = splogin.getString("customer_id", "");
        final String vendor_type = splogin.getString("vendor_type", "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (id.equals("")){
                    Intent login = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(login);
                    finish();
                    overridePendingTransition(0,0);
                }
                else {
                    if (vendor_type.equals("1")){
                        Intent login = new Intent(getApplicationContext(),DashBoardActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }else if (vendor_type.equals("2")){
                        Intent login = new Intent(getApplicationContext(),DashBoardSellerActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }else if (vendor_type.equals("3")){
                        Intent login = new Intent(getApplicationContext(),DashBoardSellerActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }

                }

            }
        },2500);
    }
}