package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class OrderDetailSellerActivity extends AppCompatActivity {

    TextView txt_name;
    String[] update_order = { "Select Order Status", "Edit", "Delete"};
    Spinner spin_update_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_seller);

        txt_name = (TextView) findViewById(R.id.txtTitle);
        spin_update_status = (Spinner) findViewById(R.id.update_status);

        txt_name.setText("Order Detail");

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,update_order);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_update_status.setAdapter(aa);
    }
}