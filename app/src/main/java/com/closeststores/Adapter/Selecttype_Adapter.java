package com.closeststores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.closeststores.Model.select_type;
import com.closeststores.R;

import java.util.ArrayList;

public class Selecttype_Adapter extends BaseAdapter {
    private ArrayList<select_type> data;
    private Activity context;
    private LayoutInflater layoutInflater = null;

    public Selecttype_Adapter(Activity context, ArrayList<select_type> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.select_type_spinner, null);
        }
        TextView txt_from;
        txt_from = (TextView) view.findViewById(R.id.txt_spinner_item);
        txt_from.setText(decodeString(data.get(position).getName()));
        return view;
    }

    public static String decodeString(String strData) {
        if (strData == null) {
            return "";
        }
        return strData.replaceAll("&lt;", "<").replaceAll("&gt;", ">")
                .replaceAll("&apos;", "'").replaceAll("&quot;", "\"")
                .replaceAll("&amp;", "&");
    }

}
