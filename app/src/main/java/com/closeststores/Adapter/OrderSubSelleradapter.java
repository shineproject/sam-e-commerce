package com.closeststores.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.R;

import java.util.ArrayList;

public class OrderSubSelleradapter extends RecyclerView.Adapter<OrderSubSelleradapter.ViewHolder> {
    private Context context;
    private ArrayList<String> data;

    public OrderSubSelleradapter(Context context, ArrayList<String> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public OrderSubSelleradapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_select_seller_list, parent, false);
        return new OrderSubSelleradapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderSubSelleradapter.ViewHolder holder, final int position) {

        holder.txt_name.setText("" + data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView) itemView.findViewById(R.id.txt_customerName);
        }
    }
}
