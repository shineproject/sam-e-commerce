package com.closeststores.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.closeststores.Model.select_secret;
import com.closeststores.R;

import java.util.ArrayList;

public class Selectsecret_Adapter extends BaseAdapter {
    private ArrayList<select_secret> data;
    private Activity context;
    private LayoutInflater layoutInflater = null;

    public Selectsecret_Adapter(Activity context, ArrayList<select_secret> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.select_type_spinner, null);
        }
        CheckedTextView txt_from;
        txt_from = (CheckedTextView) view.findViewById(R.id.txt_spinner_item);
        txt_from.setText(data.get(position).getName());
        return view;
    }

}

