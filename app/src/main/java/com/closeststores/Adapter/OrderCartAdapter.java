package com.closeststores.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.R;

import java.util.ArrayList;

public class OrderCartAdapter extends RecyclerView.Adapter<OrderCartAdapter.ViewHolder> {
    private Context context;
    private ArrayList<String> data;

    public OrderCartAdapter(Context context, ArrayList<String> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public OrderCartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_cart_list, parent, false);
        return new OrderCartAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderCartAdapter.ViewHolder holder, final int position) {

        holder.txt_name.setText("" + data.get(position));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_name = (TextView) itemView.findViewById(R.id.txt_name);
        }
    }
}
