package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.closeststores.Adapter.AddressListAdapter;
import com.closeststores.Adapter.WishListAdapter;

import java.util.ArrayList;

public class AddressListActivity extends AppCompatActivity {

    RecyclerView rv_address_list;
    AddressListAdapter addressListAdapter;
    ArrayList<String> addressList = new ArrayList<>();

    TextView txt_name;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        rv_address_list = (RecyclerView) findViewById(R.id.rv_addresslist);
        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Address List");

       addressList.add("5 Floor, ABC Tower, Naroda, ahmedabad");
       addressList.add("nilkanth society, surat");
       addressList.add("2 Floor, Makarba, Ahmedabad");

        addressListAdapter = new AddressListAdapter(getApplicationContext(), addressList);
        rv_address_list.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayout.VERTICAL, false));
        rv_address_list.setAdapter(addressListAdapter);
    }
}