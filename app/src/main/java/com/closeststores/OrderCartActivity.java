package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.closeststores.Adapter.OrderCartAdapter;

import java.util.ArrayList;

public class OrderCartActivity extends AppCompatActivity {

    RecyclerView rv_order_cart;
    OrderCartAdapter orderCartAdapter;
    ArrayList<String> orderCart = new ArrayList<>();

    TextView txt_name;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_cart);

        rv_order_cart = (RecyclerView) findViewById(R.id.rv_order_cart);
        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Order Cart");

        orderCart.add("Man Clothes");
        orderCart.add("Ladies Clothes");
        orderCart.add("Kids Clothes");
        orderCart.add("Perfumes");

        orderCartAdapter = new OrderCartAdapter(getApplicationContext(), orderCart);
        rv_order_cart.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayout.VERTICAL, false));
        rv_order_cart.setAdapter(orderCartAdapter);
    }
}