package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.closeststores.Adapter.Selectdelivery_Adapter;
import com.closeststores.Adapter.Selectrefund_Adapter;
import com.closeststores.Adapter.Selectsecret_Adapter;
import com.closeststores.Model.select_delivery;
import com.closeststores.Model.select_refund;
import com.closeststores.Model.select_secret;
import com.closeststores.Services.ApiConfig;
import com.closeststores.Utils.ConnectivityDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterThirdActivity extends AppCompatActivity {

    Spinner spin_secret_qus, spin_delivery_qus, spin_refund_qus, spin_what_distance, spin_minimum_limitation, spin_paid_delivery,
            spin_preffered_delivery, spin_outside_city, spin_refund_answer;
    JSONArray jsonArraySecret;
    select_secret sel_secret;
    Button btn_submit;

    EditText et_secret, et_preferred_name, et_preferred_contact, et_outside_name, et_outside_contact;

    Selectsecret_Adapter select_secret_adapter;

    ArrayList<select_secret> data_secret = new ArrayList<>();

    String str_secret = "", str_delivery = "", str_refund = "";

    String str_select_type = "";

    String first = "", last = "", user = "", email = "", phone = "", otp = "", address = "", city = "", state = "", pincode = "",
            shop_name = "", reg_no = "", owner_name = "", owner_contact = "", cor_name = "", cor_contact = "", image_path = ""
            , password = "", str_category_type_name = "",
            str_category_type_name_sub = "", Str_category_type_name_sub_sub = "", responseimage = "", str_secret_ans = "", city_free_deli = "", city_free_deli_distance = "",
            city_free_deli_limit = "", city_paid_deli = "", city_prefr_vendor_deli = "", city_prefr_vendor_name = "", city_prefr_vendor_contact = "",
            outcity_supply = "", outcity_prefr_vendor_name = "", outcity_prefr_vendor_contact = "", retrn_policy = "", retrn_policy_provide_info = "";

    LinearLayout l_free_delivery, l_minimum, l_preferred;
    String[] do_you_provide_free = { "Do you provide free delivery?", "Yes", "No Delivery"};
    String[] what_distance_you_provide_free = { "What distance (KM) you provide free delivery.", "Free within 500 meter", "Free within 1 Kms","Free within 2 Kms", "Free within 3 Kms"};
    String[] minimum_limitation = { "Any minimum bill limitation for free delivery?", "No limit for 500 KM", "Rs 50 for 1 KM", "Rs 200 for 2 KMs", "Rs 500 for 3 KMs"};
    String[] do_you_provide_paid = { "Do you provide paid delivery?", "Yes", "No Delivery"};
    String[] do_you_preferred_delivery = { "Do you only use preferred vendor for delivery?", "Yes", "No, Ok with any vendor"};
    String[] do_you_outcity = { "Do you supply only in city or state or anywhere in india?", "Yes", "No, Other parts of my state", "No, anywhere in India"};
    String[] do_you_have_refund = { "Do you have return and refund policy?", "Yes Return and Refund", "Yes Return but Reuse", "No"};
    String[] refund_answer = { "If yes then provide info.", "Return and Refund is allowed for 5 Days", "Return and exchange or buy anything within 7 Days"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_third);

        Intent i = getIntent();
        str_select_type = i.getStringExtra("select_type");
        first = i.getStringExtra("first");
        last = i.getStringExtra("last");
        user = i.getStringExtra("username");
        email = i.getStringExtra("email");
        phone = i.getStringExtra("phone");
        otp = i.getStringExtra("otp");
        address = i.getStringExtra("address");
        city = i.getStringExtra("city");
        state = i.getStringExtra("state");
        pincode = i.getStringExtra("pincode");
        shop_name = i.getStringExtra("shop_name");
        reg_no = i.getStringExtra("reg_no");
        owner_name = i.getStringExtra("owner_name");
        owner_contact = i.getStringExtra("owner_contact");
        cor_name = i.getStringExtra("cor_name");
        cor_contact = i.getStringExtra("cor_contact");
        image_path = i.getStringExtra("image");
        password = i.getStringExtra("password");
        str_category_type_name = i.getStringExtra("category");
        str_category_type_name_sub = i.getStringExtra("sub_category");
        Str_category_type_name_sub_sub = i.getStringExtra("sub_sub_category");

        spin_secret_qus = (Spinner) findViewById(R.id.spin_secret_question);
        spin_delivery_qus = (Spinner) findViewById(R.id.spin_delivery_question);
        spin_refund_qus = (Spinner) findViewById(R.id.spin_refund_question);
        et_secret = (EditText) findViewById(R.id.et_secret_qus);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        spin_what_distance = (Spinner) findViewById(R.id.spin_distance_free_delivery);
        spin_minimum_limitation = (Spinner) findViewById(R.id.spin_minimum_limitation);
        spin_paid_delivery = (Spinner) findViewById(R.id.spin_paid_delivery);
        spin_preffered_delivery = (Spinner) findViewById(R.id.spin_preffered_delivery);
        l_free_delivery = (LinearLayout) findViewById(R.id.l_distance_free);
        l_minimum = (LinearLayout) findViewById(R.id.l_minimum_distance);
        l_preferred = (LinearLayout) findViewById(R.id.l_preferred_delivery);
        et_preferred_name = (EditText) findViewById(R.id.et_preferred_name);
        et_preferred_contact = (EditText) findViewById(R.id.et_preferred_contact);
        spin_outside_city = (Spinner) findViewById(R.id.spin_outside_city);
        et_outside_name = (EditText) findViewById(R.id.et_outside_name);
        et_outside_contact = (EditText) findViewById(R.id.et_outside_contact);
        spin_refund_answer = (Spinner) findViewById(R.id.spin_refund_answer);

        new showselectqus().execute();

        if (str_select_type.equals("1")){
            spin_secret_qus.setVisibility(View.VISIBLE);
            spin_delivery_qus.setVisibility(View.GONE);
            spin_refund_qus.setVisibility(View.GONE);
        }else if (str_select_type.equals("2")){
            spin_secret_qus.setVisibility(View.VISIBLE);
            spin_delivery_qus.setVisibility(View.VISIBLE);
            spin_refund_qus.setVisibility(View.VISIBLE);
        }else if (str_select_type.equals("3")){
            spin_secret_qus.setVisibility(View.VISIBLE);
            spin_delivery_qus.setVisibility(View.VISIBLE);
            spin_refund_qus.setVisibility(View.VISIBLE);
        }

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,do_you_provide_free);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_delivery_qus.setAdapter(aa);

        ArrayAdapter aa1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,what_distance_you_provide_free);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_what_distance.setAdapter(aa1);

        ArrayAdapter aa2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,minimum_limitation);
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_minimum_limitation.setAdapter(aa2);

        ArrayAdapter aa3 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,do_you_provide_paid);
        aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_paid_delivery.setAdapter(aa3);

        ArrayAdapter aa4 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,do_you_preferred_delivery);
        aa4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_preffered_delivery.setAdapter(aa4);

        ArrayAdapter aa5 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,do_you_outcity);
        aa5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_outside_city.setAdapter(aa5);

        ArrayAdapter aa6 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,do_you_have_refund);
        aa6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_refund_qus.setAdapter(aa6);

        ArrayAdapter aa7 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,refund_answer);
        aa7.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_refund_answer.setAdapter(aa7);

        spin_secret_qus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (data_secret.get(position).getName().equals("Select Secret Question")) {
                    str_secret = "Select Secret Question";
                    et_secret.setText("");
                    et_secret.setVisibility(View.GONE);
                } else {
                    str_secret = data_secret.get(position).getId();
                    et_secret.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_delivery_qus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (do_you_provide_free[position].equals("Do you provide free delivery?")) {
                    city_free_deli = "";
                    spin_what_distance.setVisibility(View.GONE);
                    spin_minimum_limitation.setVisibility(View.GONE);
                    l_free_delivery.setVisibility(View.GONE);
                    l_minimum.setVisibility(View.GONE);
                } else if (do_you_provide_free[position].equals("Yes")) {
                    city_free_deli = do_you_provide_free[position];
                    spin_what_distance.setVisibility(View.VISIBLE);
                    spin_minimum_limitation.setVisibility(View.GONE);
                    l_free_delivery.setVisibility(View.VISIBLE);
                    l_minimum.setVisibility(View.GONE);
                }
                else {
                    city_free_deli = do_you_provide_free[position];
                    str_delivery = do_you_provide_free[position];
                    spin_what_distance.setVisibility(View.GONE);
                    spin_minimum_limitation.setVisibility(View.GONE);
                    l_free_delivery.setVisibility(View.GONE);
                    l_minimum.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_what_distance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (what_distance_you_provide_free[position].equals("What distance (KM) you provide free delivery.")) {
                    city_free_deli_distance = "";
                    spin_minimum_limitation.setVisibility(View.GONE);
                    l_minimum.setVisibility(View.GONE);
                } else {
                    city_free_deli_distance = what_distance_you_provide_free[position];
                    spin_minimum_limitation.setVisibility(View.VISIBLE);
                    l_minimum.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_minimum_limitation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (minimum_limitation[position].equals("Any minimum bill limitation for free delivery?")) {
                    city_free_deli_limit = "";
                } else {
                    city_free_deli_limit = minimum_limitation[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_paid_delivery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (do_you_provide_paid[position].equals("Do you provide paid delivery?")) {
                    city_paid_deli = "";
                    spin_preffered_delivery.setVisibility(View.GONE);
                    l_preferred.setVisibility(View.GONE);
                    et_preferred_name.setVisibility(View.GONE);
                    et_preferred_contact.setVisibility(View.GONE);
                }else if (do_you_provide_free[position].equals("Yes")) {
                    city_paid_deli = do_you_provide_paid[position];
                    spin_preffered_delivery.setVisibility(View.VISIBLE);
                    l_preferred.setVisibility(View.VISIBLE);
                }else {
                    city_paid_deli = do_you_provide_paid[position];
                    spin_preffered_delivery.setVisibility(View.GONE);
                    l_preferred.setVisibility(View.GONE);
                    et_preferred_name.setVisibility(View.GONE);
                    et_preferred_contact.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_preffered_delivery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (do_you_preferred_delivery[position].equals("Do you only use preferred vendor for delivery?")) {
                    city_prefr_vendor_deli = "";
                    et_preferred_name.setVisibility(View.GONE);
                    et_preferred_contact.setVisibility(View.GONE);
                }else if (do_you_preferred_delivery[position].equals("Yes")) {
                    city_prefr_vendor_deli = do_you_preferred_delivery[position];
                    et_preferred_name.setVisibility(View.VISIBLE);
                    et_preferred_contact.setVisibility(View.VISIBLE);
                }else {
                    city_prefr_vendor_deli = do_you_preferred_delivery[position];
                    et_preferred_name.setVisibility(View.GONE);
                    et_preferred_contact.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_outside_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (do_you_outcity[position].equals("Do you supply only in city or state or anywhere in india?")) {
                    outcity_supply = "";
                    et_outside_name.setVisibility(View.GONE);
                    et_outside_contact.setVisibility(View.GONE);
                }else if (do_you_outcity[position].equals("Yes")) {
                    outcity_supply = do_you_outcity[position];
                    et_outside_name.setVisibility(View.VISIBLE);
                    et_outside_contact.setVisibility(View.VISIBLE);
                }else {
                    outcity_supply = do_you_outcity[position];
                    et_outside_name.setVisibility(View.GONE);
                    et_outside_contact.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_refund_qus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (do_you_have_refund[position].equals("Do you have return and refund policy?")) {
                    retrn_policy = "";
                    spin_refund_answer.setVisibility(View.GONE);
                } else if (do_you_have_refund[position].equals("No")){
                    retrn_policy = do_you_have_refund[position];
                    spin_refund_answer.setVisibility(View.GONE);
                }else {
                    retrn_policy = do_you_have_refund[position];
                    spin_refund_answer.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_refund_answer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (refund_answer[position].equals("If yes then provide info.")) {
                    retrn_policy_provide_info = "";
                } else {
                    retrn_policy_provide_info = refund_answer[position];
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_select_type.equals("1")){
                    if (et_secret.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                    }else{
                        str_secret_ans = et_secret.getText().toString().trim();
                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                            new Signup().execute();
                        }
                        else {
                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else if (str_select_type.equals("2")){
                    if (et_secret.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                    }else if (city_free_deli.equals("")){
                        Toast.makeText(RegisterThirdActivity.this, "Please select free delivery", Toast.LENGTH_LONG).show();
                    }else if (city_free_deli.equals("Yes")){
                            if (city_free_deli_distance.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select what distance you provide free delivery", Toast.LENGTH_LONG).show();
                            }else if (city_free_deli_limit.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select any minimum bill limitation for free delivery", Toast.LENGTH_LONG).show();
                            }else {
                                if (city_paid_deli.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select provide paid delivery", Toast.LENGTH_LONG).show();
                                } else if (city_paid_deli.equals("Yes")){
                                    if (city_prefr_vendor_deli.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select preferred vendor for delivery", Toast.LENGTH_LONG).show();
                                    }else if (city_prefr_vendor_deli.equals("Yes")){
                                        if (et_preferred_name.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select preferred name", Toast.LENGTH_LONG).show();
                                        }else if (et_preferred_contact.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select preferred contact", Toast.LENGTH_LONG).show();
                                        }else {
                                            if (outcity_supply.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                            }
                                            else if (outcity_supply.equals("Yes")){
                                                if (et_outside_name.getText().toString().trim().equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                                }else if (et_outside_contact.getText().toString().trim().equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                                }else {
                                                    if (retrn_policy.equals("")){
                                                        Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                                    }else if (retrn_policy.equals("No")){
                                                        str_secret_ans = et_secret.getText().toString().trim();
                                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                            new Signup().execute();
                                                        }
                                                        else {
                                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                        }
                                                    }else {
                                                        if (retrn_policy_provide_info.equals("")){
                                                            Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                        }else {
                                                            str_secret_ans = et_secret.getText().toString().trim();
                                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                                new Signup().execute();
                                                            }
                                                            else {
                                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    }
                                                }
                                            }else {
                                                if (retrn_policy.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                                }else if (retrn_policy.equals("No")){
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }else {
                                                    if (retrn_policy_provide_info.equals("")){
                                                        Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                    }else {
                                                        str_secret_ans = et_secret.getText().toString().trim();
                                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                            new Signup().execute();
                                                        }
                                                        else {
                                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }else if (city_prefr_vendor_deli.equals("No, Ok with any vendor")){
                                        if (outcity_supply.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                        }
                                        else if (outcity_supply.equals("Yes")){
                                            if (et_outside_name.getText().toString().trim().equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                            }else if (et_outside_contact.getText().toString().trim().equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                            }

                                        }else {
                                            if (retrn_policy.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                            }else if (retrn_policy.equals("No")){
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }else {
                                                if (retrn_policy_provide_info.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                }else {
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else if (city_paid_deli.equals("No Delivery")){
                                    if (outcity_supply.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                    }
                                    else if (outcity_supply.equals("Yes")){
                                        if (et_outside_name.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                        }else if (et_outside_contact.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                        }

                                    }else {
                                        if (retrn_policy.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                        }else if (retrn_policy.equals("No")){
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }else {
                                            if (retrn_policy_provide_info.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                            }else {
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                    }else if (city_free_deli.equals("No Delivery")){
                       if (city_paid_deli.equals("")){
                            Toast.makeText(RegisterThirdActivity.this, "Please select provide paid delivery", Toast.LENGTH_LONG).show();
                        } else if (city_paid_deli.equals("Yes")){
                            if (city_prefr_vendor_deli.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select preferred vendor for delivery", Toast.LENGTH_LONG).show();
                            }else if (city_prefr_vendor_deli.equals("Yes")){
                                if (et_preferred_name.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select preferred name", Toast.LENGTH_LONG).show();
                                }else if (et_preferred_contact.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select preferred contact", Toast.LENGTH_LONG).show();
                                }else {
                                    if (outcity_supply.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                    }
                                    else if (outcity_supply.equals("Yes")){
                                        if (et_outside_name.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                        }else if (et_outside_contact.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                        }else {
                                            if (retrn_policy.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                            }else if (retrn_policy.equals("No")){
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }else {
                                                if (retrn_policy_provide_info.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                }else {
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        }

                                    }else {
                                        if (retrn_policy.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                        }else if (retrn_policy.equals("No")){
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }else {
                                            if (retrn_policy_provide_info.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                            }else {
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if (city_prefr_vendor_deli.equals("No, Ok with any vendor")){
                                if (outcity_supply.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                }
                                else if (outcity_supply.equals("Yes")){
                                    if (et_outside_name.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                    }else if (et_outside_contact.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                    }

                                }else {
                                    if (retrn_policy.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                    }else if (retrn_policy.equals("No")){
                                        str_secret_ans = et_secret.getText().toString().trim();
                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                            new Signup().execute();
                                        }
                                        else {
                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                        }
                                    }else {
                                        if (retrn_policy_provide_info.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                        }else {
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                }
                            }
                        }else if (city_paid_deli.equals("No Delivery")){
                           if (outcity_supply.equals("")){
                               Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                           }
                           else if (outcity_supply.equals("Yes")){
                               if (et_outside_name.getText().toString().trim().equals("")){
                                   Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                               }else if (et_outside_contact.getText().toString().trim().equals("")){
                                   Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                               }

                           }else {
                               if (retrn_policy.equals("")){
                                   Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                               }else if (retrn_policy.equals("No")){
                                   str_secret_ans = et_secret.getText().toString().trim();
                                   if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                       new Signup().execute();
                                   }
                                   else {
                                       Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                   }
                               }else {
                                   if (retrn_policy_provide_info.equals("")){
                                       Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                   }else {
                                       str_secret_ans = et_secret.getText().toString().trim();
                                       if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                           new Signup().execute();
                                       }
                                       else {
                                           Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                       }
                                   }
                               }
                           }
                       }
                    }
                }
                else if (str_select_type.equals("3")){
                    if (et_secret.getText().toString().trim().equals("")){
                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.please_enter_secret_qus), Toast.LENGTH_LONG).show();
                    }else if (city_free_deli.equals("")){
                        Toast.makeText(RegisterThirdActivity.this, "Please select free delivery", Toast.LENGTH_LONG).show();
                    }else if (city_free_deli.equals("Yes")){
                        if (city_free_deli_distance.equals("")){
                            Toast.makeText(RegisterThirdActivity.this, "Please select what distance you provide free delivery", Toast.LENGTH_LONG).show();
                        }else if (city_free_deli_limit.equals("")){
                            Toast.makeText(RegisterThirdActivity.this, "Please select any minimum bill limitation for free delivery", Toast.LENGTH_LONG).show();
                        }else {
                            if (city_paid_deli.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select provide paid delivery", Toast.LENGTH_LONG).show();
                            } else if (city_paid_deli.equals("Yes")){
                                if (city_prefr_vendor_deli.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select preferred vendor for delivery", Toast.LENGTH_LONG).show();
                                }else if (city_prefr_vendor_deli.equals("Yes")){
                                    if (et_preferred_name.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select preferred name", Toast.LENGTH_LONG).show();
                                    }else if (et_preferred_contact.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select preferred contact", Toast.LENGTH_LONG).show();
                                    }else {
                                        if (outcity_supply.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                        }
                                        else if (outcity_supply.equals("Yes")){
                                            if (et_outside_name.getText().toString().trim().equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                            }else if (et_outside_contact.getText().toString().trim().equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                            }else {
                                                if (retrn_policy.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                                }else if (retrn_policy.equals("No")){
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }else {
                                                    if (retrn_policy_provide_info.equals("")){
                                                        Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                    }else {
                                                        str_secret_ans = et_secret.getText().toString().trim();
                                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                            new Signup().execute();
                                                        }
                                                        else {
                                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }
                                            }
                                        }else {
                                            if (retrn_policy.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                            }else if (retrn_policy.equals("No")){
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }else {
                                                if (retrn_policy_provide_info.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                }else {
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else if (city_prefr_vendor_deli.equals("No, Ok with any vendor")){
                                    if (outcity_supply.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                    }
                                    else if (outcity_supply.equals("Yes")){
                                        if (et_outside_name.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                        }else if (et_outside_contact.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                        }

                                    }else {
                                        if (retrn_policy.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                        }else if (retrn_policy.equals("No")){
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }else {
                                            if (retrn_policy_provide_info.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                            }else {
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if (city_paid_deli.equals("No Delivery")){
                                if (outcity_supply.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                }
                                else if (outcity_supply.equals("Yes")){
                                    if (et_outside_name.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                    }else if (et_outside_contact.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                    }

                                }else {
                                    if (retrn_policy.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                    }else if (retrn_policy.equals("No")){
                                        str_secret_ans = et_secret.getText().toString().trim();
                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                            new Signup().execute();
                                        }
                                        else {
                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                        }
                                    }else {
                                        if (retrn_policy_provide_info.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                        }else {
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else if (city_free_deli.equals("No Delivery")){
                        if (city_paid_deli.equals("")){
                            Toast.makeText(RegisterThirdActivity.this, "Please select provide paid delivery", Toast.LENGTH_LONG).show();
                        } else if (city_paid_deli.equals("Yes")){
                            if (city_prefr_vendor_deli.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select preferred vendor for delivery", Toast.LENGTH_LONG).show();
                            }else if (city_prefr_vendor_deli.equals("Yes")){
                                if (et_preferred_name.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select preferred name", Toast.LENGTH_LONG).show();
                                }else if (et_preferred_contact.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select preferred contact", Toast.LENGTH_LONG).show();
                                }else {
                                    if (outcity_supply.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                    }
                                    else if (outcity_supply.equals("Yes")){
                                        if (et_outside_name.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                        }else if (et_outside_contact.getText().toString().trim().equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                        }else {
                                            if (retrn_policy.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                            }else if (retrn_policy.equals("No")){
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }else {
                                                if (retrn_policy_provide_info.equals("")){
                                                    Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                                }else {
                                                    str_secret_ans = et_secret.getText().toString().trim();
                                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                        new Signup().execute();
                                                    }
                                                    else {
                                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            }
                                        }

                                    }else {
                                        if (retrn_policy.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                        }else if (retrn_policy.equals("No")){
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }else {
                                            if (retrn_policy_provide_info.equals("")){
                                                Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                            }else {
                                                str_secret_ans = et_secret.getText().toString().trim();
                                                if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                    new Signup().execute();
                                                }
                                                else {
                                                    Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }
                                    }
                                }
                            }else if (city_prefr_vendor_deli.equals("No, Ok with any vendor")){
                                if (outcity_supply.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                                }
                                else if (outcity_supply.equals("Yes")){
                                    if (et_outside_name.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                    }else if (et_outside_contact.getText().toString().trim().equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                    }

                                }else {
                                    if (retrn_policy.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                    }else if (retrn_policy.equals("No")){
                                        str_secret_ans = et_secret.getText().toString().trim();
                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                            new Signup().execute();
                                        }
                                        else {
                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                        }
                                    }else {
                                        if (retrn_policy_provide_info.equals("")){
                                            Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                        }else {
                                            str_secret_ans = et_secret.getText().toString().trim();
                                            if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                                new Signup().execute();
                                            }
                                            else {
                                                Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                }
                            }
                        }else if (city_paid_deli.equals("No Delivery")){
                            if (outcity_supply.equals("")){
                                Toast.makeText(RegisterThirdActivity.this, "Please select outcity anywhere in india", Toast.LENGTH_LONG).show();
                            }
                            else if (outcity_supply.equals("Yes")){
                                if (et_outside_name.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select name", Toast.LENGTH_LONG).show();
                                }else if (et_outside_contact.getText().toString().trim().equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select contact", Toast.LENGTH_LONG).show();
                                }

                            }else {
                                if (retrn_policy.equals("")){
                                    Toast.makeText(RegisterThirdActivity.this, "Please select return policy", Toast.LENGTH_LONG).show();
                                }else if (retrn_policy.equals("No")){
                                    str_secret_ans = et_secret.getText().toString().trim();
                                    if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                        new Signup().execute();
                                    }
                                    else {
                                        Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                    }
                                }else {
                                    if (retrn_policy_provide_info.equals("")){
                                        Toast.makeText(RegisterThirdActivity.this, "Please select provide info", Toast.LENGTH_LONG).show();
                                    }else {
                                        str_secret_ans = et_secret.getText().toString().trim();
                                        if (ConnectivityDetector.isConnectingToInternet(RegisterThirdActivity.this)) {
                                            new Signup().execute();
                                        }
                                        else {
                                            Toast.makeText(RegisterThirdActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    public class showselectqus extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterThirdActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.QUESTION_LIST);
                JSONObject postDataParams = new JSONObject();

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            QusResponse(result);
        }
    }

    private void QusResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");
                String data = jsobjectcategory.getString("data");

                if (status.equals("1")) {

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        jsonArraySecret = jsonObject.getJSONArray("secretquestionlist");

                        if (jsonArraySecret.length() > 0){
                            sel_secret = new select_secret();
                            sel_secret.setId("");
                            sel_secret.setName("Select Secret Question");
                            data_secret.add(sel_secret);

                            for (int i = 0; i < jsonArraySecret.length(); i++) {
                                JSONObject jsonObject1 = jsonArraySecret.getJSONObject(i);

                                sel_secret = new select_secret();

                                String Use_Id = jsonObject1.getString("id");
                                String Use_Name = jsonObject1.getString("name");

                                sel_secret.setId(Use_Id);
                                sel_secret.setName(Use_Name);
                                data_secret.add(sel_secret);
                            }

                            select_secret_adapter = new Selectsecret_Adapter(RegisterThirdActivity.this, data_secret);
                                spin_secret_qus.setAdapter(select_secret_adapter);
                        }

//                        jsonArrayDelivery = jsonObject.getJSONArray("deliveryquestionlist");
//
//                        if (jsonArrayDelivery.length() > 0){
//                            sel_delivery = new select_delivery();
//                            sel_delivery.setId("");
//                            sel_delivery.setName("Select Delivery Question");
//                            data_delivery.add(sel_delivery);
//
//                            for (int i = 0; i < jsonArrayDelivery.length(); i++) {
//                                JSONObject jsonObject1 = jsonArrayDelivery.getJSONObject(i);
//
//                                sel_delivery = new select_delivery();
//
//                                String Use_Id = jsonObject1.getString("id");
//                                String Use_Name = jsonObject1.getString("name");
//
//                                sel_delivery.setId(Use_Id);
//                                sel_delivery.setName(Use_Name);
//                                data_delivery.add(sel_delivery);
//                            }
//
//                            select_delivery_adapter = new Selectdelivery_Adapter(RegisterThirdActivity.this, data_delivery);
//                            spin_delivery_qus.setAdapter(select_delivery_adapter);
//                        }
//
//                        jsonArrayRefund = jsonObject.getJSONArray("refundquestionlist");
//
//                        if (jsonArrayRefund.length() > 0){
//                            sel_refund = new select_refund();
//                            sel_refund.setId("");
//                            sel_refund.setName("Select Refund Question");
//                            data_refund.add(sel_refund);
//
//                            for (int i = 0; i < jsonArrayRefund.length(); i++) {
//                                JSONObject jsonObject1 = jsonArrayRefund.getJSONObject(i);
//
//                                sel_refund = new select_refund();
//
//                                String Use_Id = jsonObject1.getString("id");
//                                String Use_Name = jsonObject1.getString("name");
//
//                                sel_refund.setId(Use_Id);
//                                sel_refund.setName(Use_Name);
//                                data_refund.add(sel_refund);
//                            }
//
//                            select_refund_adapter = new Selectrefund_Adapter(RegisterThirdActivity.this, data_refund);
//                            spin_refund_qus.setAdapter(select_refund_adapter);
//                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(RegisterThirdActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Signup Api call Multipart
    @SuppressLint("StaticFieldLeak")
    private class Signup extends AsyncTask<String, String, String> {

        private ProgressDialog Dialog = new ProgressDialog(RegisterThirdActivity.this);

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... params) {

            MultipartBody.Builder builder = new MultipartBody.Builder();

            builder.setType(MultipartBody.FORM);
            final MediaType MEDIA_TYPE = MediaType.parse("image/jpg");
            if (image_path.equals("")){

            }
            else {
                builder.addFormDataPart("vendor_image", image_path, RequestBody.create(MEDIA_TYPE, new File(image_path)));
            }
            builder.addFormDataPart("first_name", first);
            builder.addFormDataPart("last_name", last);
            builder.addFormDataPart("vendor_type",str_select_type);
            builder.addFormDataPart("user_name", user);
            builder.addFormDataPart("email", email);
            builder.addFormDataPart("phone_number", phone);
            builder.addFormDataPart("otp", otp);
            builder.addFormDataPart("city", city);
            builder.addFormDataPart("state", state);
            builder.addFormDataPart("address", address);
            builder.addFormDataPart("business_category", str_category_type_name);
            builder.addFormDataPart("pin_code", pincode);
            builder.addFormDataPart("vendor_name", shop_name);
            builder.addFormDataPart("owner_name", owner_name);
            builder.addFormDataPart("owner_contact", owner_contact);
            builder.addFormDataPart("coordinator_name", cor_name);
            builder.addFormDataPart("coordinator_contact", cor_contact);
            builder.addFormDataPart("shop_reg_id", reg_no);
            builder.addFormDataPart("password", password);
            builder.addFormDataPart("sub_category_multi_variaties", str_category_type_name_sub);
            builder.addFormDataPart("city_free_deli", city_free_deli);
            builder.addFormDataPart("city_free_deli_distance", city_free_deli_distance);
            builder.addFormDataPart("city_free_deli_limit", city_free_deli_limit);
            builder.addFormDataPart("city_paid_deli", city_paid_deli);
            builder.addFormDataPart("city_prefr_vendor_deli", city_prefr_vendor_deli);
            builder.addFormDataPart("city_prefr_vendor_name", et_preferred_name.getText().toString().trim());
            builder.addFormDataPart("city_prefr_vendor_contact", et_preferred_contact.getText().toString().trim());
            builder.addFormDataPart("outcity_supply", outcity_supply);
            builder.addFormDataPart("outcity_prefr_vendor_name", et_outside_name.getText().toString().trim());
            builder.addFormDataPart("outcity_prefr_vendor_contact", et_outside_contact.getText().toString().trim());
            builder.addFormDataPart("retrn_policy", retrn_policy);
            builder.addFormDataPart("retrn_policy_provide_info", retrn_policy_provide_info);
            builder.addFormDataPart("secret_question_id", str_secret);
            builder.addFormDataPart("secret_question_answer", et_secret.getText().toString().trim());

            Log.e("params", builder.toString());
            RequestBody requestBody = builder.build();

            try {

                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder()
                        .url(ApiConfig.REGISTER)
                        .post(requestBody)
                        .build();
                Response resp = client.newCall(request).execute();
                responseimage = resp.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            Dialog.dismiss();
            Log.e("Response",responseimage);
            Deliverydataimage(responseimage);
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

    }

    private void Deliverydataimage(String response) {
        try {
            if (!response.equals("")) {

                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {
                    Toast.makeText(RegisterThirdActivity.this, message, Toast.LENGTH_LONG).show();
                    Intent register = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(register);
                    finish();
                    overridePendingTransition(0,0);
                }
                else {
                    Toast.makeText(RegisterThirdActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}