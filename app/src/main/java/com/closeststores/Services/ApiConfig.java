package com.closeststores.Services;

public class ApiConfig {

    static String SERVICE_URL = "http://closeststores.com/index.php?route=api/";  // Main URL
//    static String SERVICE_URL = "http://justbeepme.in/index.php?route=api/";

    public static String Login_URL = SERVICE_URL+"newtest";
    public static String SEND_OTP = SERVICE_URL+"sendotp";
    public static String MAIN_CATEGORY = SERVICE_URL+"maincategory";
    public static String MAIN_CATEGORY_SUB = SERVICE_URL+"subcategory";
    public static String MAIN_CATEGORY_SUB_SUB = SERVICE_URL+"getSubSubCategory";
    public static String QUESTION_LIST = SERVICE_URL+"questionlist";
    public static String VENDOR_TYPE = SERVICE_URL+"vendortype";
    public static String REGISTER = SERVICE_URL+"register";
    public static String FORGOT_PASSWORD = SERVICE_URL+"forgotpassword";
}
