package com.closeststores.Services;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.Toast;

import com.closeststores.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class util {
    public static boolean checkLocationService(Context mContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                int locationMode = Settings.Secure.getInt(mContext.getContentResolver(), Settings.Secure.LOCATION_MODE);
                return (locationMode != Settings.Secure.LOCATION_MODE_OFF
                        && (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY
                        || locationMode == Settings.Secure.LOCATION_MODE_BATTERY_SAVING)); //check location mode
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            String locationProviders = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
        return false;
    }

    public static boolean connectionAvailable(final Context c) {

        ConnectivityManager cm = (ConnectivityManager) c
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null && cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isConnected())
            return true;
        else {
            ((Activity) c).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(c,
                            c.getString(R.string.no_internet),
                            Toast.LENGTH_LONG).show();
                }
            });
            return false;
        }
    }

    public static String encodeURL(String query) throws UnsupportedEncodingException {
        return URLEncoder.encode(query, "utf-8");
    }

    public static int dpToPx(Resources r, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    // function to make url for drawing path on google map.
    public static String buildURL(double sourcelat, double sourcelog, double destlat, double destlog) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString(sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString(destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }
}
