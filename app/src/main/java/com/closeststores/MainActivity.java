package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.closeststores.Services.ApiConfig;
import com.closeststores.Utils.ConnectivityDetector;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    TextView txt_register, txt_forgot;
    Button btnlogin;
    TextInputEditText et_username, et_mobile, et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_register = findViewById(R.id.txt_register);
        txt_forgot = findViewById(R.id.txt_forgot);
        btnlogin = findViewById(R.id.btn_login);
        et_username = findViewById(R.id.username);
        et_mobile = findViewById(R.id.email);
        et_password = findViewById(R.id.password);

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(register);
            }
        });

        txt_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgot = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(forgot);
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_username.getText().toString().trim().equals("")) {
                    Toast.makeText(MainActivity.this, getString(R.string.please_enter_name), Toast.LENGTH_LONG).show();
                } else if (et_mobile.getText().toString().trim().equals("")) {
                    Toast.makeText(MainActivity.this, getString(R.string.please_enter_mobile), Toast.LENGTH_LONG).show();
                }else if (!et_mobile.getText().toString().trim().equals("")) {
                    if (TextUtils.isDigitsOnly(et_mobile.getText())){
                        if (et_password.getText().toString().trim().equals("")){
                            Toast.makeText(MainActivity.this, getString(R.string.please_enter_password), Toast.LENGTH_LONG).show();
                        }else {
                            if (ConnectivityDetector.isConnectingToInternet(MainActivity.this)) {
                                new Login().execute();
                            } else {
                                Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                            }
                        }
                    }else{
                        if (!Patterns.EMAIL_ADDRESS.matcher(et_mobile.getText().toString()).matches()) {
                            Toast.makeText(MainActivity.this, getString(R.string.please_enter_valid), Toast.LENGTH_LONG).show();
                        }else {
                            if (et_password.getText().toString().trim().equals("")){
                                Toast.makeText(MainActivity.this, getString(R.string.please_enter_password), Toast.LENGTH_LONG).show();
                            }else {
                                if (ConnectivityDetector.isConnectingToInternet(MainActivity.this)) {
                                    new Login().execute();
                                } else {
                                    Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                    }
                }
                else if (et_password.getText().toString().trim().equals("")){
                    Toast.makeText(MainActivity.this, getString(R.string.please_enter_password), Toast.LENGTH_LONG).show();
                }else {
                    if (ConnectivityDetector.isConnectingToInternet(MainActivity.this)) {
                        new Login().execute();
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    // Login Api Call
    @SuppressLint("StaticFieldLeak")
    public class Login extends AsyncTask<String, Void, String> {

        private ProgressDialog Dialog = new ProgressDialog(MainActivity.this);

        protected void onPreExecute() {
            Dialog.show();
            Dialog.setMessage(getResources().getString(R.string.please_wait));
            Dialog.setCancelable(false);
        }

        @SuppressLint("WrongThread")
        protected String doInBackground(String... arg0) {
            try {

                URL url = new URL(ApiConfig.Login_URL);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("username", et_username.getText().toString().trim());
                postDataParams.put("email", et_mobile.getText().toString().trim());
                postDataParams.put("password", et_password.getText().toString().trim());

                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Dialog.dismiss();
            LoginResponse(result);
        }
    }

    private void LoginResponse(String response) {

        if (!response.equals("")) {
            try {
                JSONObject jsobjectcategory = new JSONObject(response);

                String status = jsobjectcategory.getString("status");
                String message = jsobjectcategory.getString("message");

                if (status.equals("1")) {

                    SharedPreferences spuser = MainActivity.this.getSharedPreferences("login", 0);
                    SharedPreferences.Editor Eduser = spuser.edit();

                    String data = jsobjectcategory.getString("data");
                    JSONObject jsonObject = new JSONObject(data);

                    String Use_Store_Id = jsonObject.getString("store_id");
                    String Use_Name = jsonObject.getString("firstname") + " " + jsonObject.getString("lastname");
                    String Use_User_Name = jsonObject.getString("username");
                    String Use_Vendor_Type = jsonObject.getString("vendor_type");
                    String Use_Vendor_Image = jsonObject.getString("vendor_image");
                    String Use_Email = jsonObject.getString("email");
                    String Use_Phone = jsonObject.getString("telephone");
                    String Use_Seller_id = jsonObject.getString("seller_id");
                    String Use_Vendor_name = jsonObject.getString("vendor_name");

                    Eduser.putString("store_id", Use_Store_Id);
                    Eduser.putString("name", Use_Name);
                    Eduser.putString("user_name", Use_User_Name);
                    Eduser.putString("vendor_type", Use_Vendor_Type);
                    Eduser.putString("vendor_image", Use_Vendor_Image);
                    Eduser.putString("email",Use_Email);
                    Eduser.putString("telephone",Use_Phone);
                    Eduser.putString("seller_id",Use_Seller_id);
                    Eduser.putString("vendor_name",Use_Vendor_name);
                    Eduser.apply();

                    if (Use_Vendor_Type.equals("1")){
                        Intent login = new Intent(getApplicationContext(),DashBoardActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }else if (Use_Vendor_Type.equals("2")){
                        Intent login = new Intent(getApplicationContext(),DashBoardSellerActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }else if (Use_Vendor_Type.equals("3")){
                        Intent login = new Intent(getApplicationContext(),DashBoardSellerActivity.class);
                        startActivity(login);
                        finish();
                        overridePendingTransition(0,0);
                    }


                }else {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}