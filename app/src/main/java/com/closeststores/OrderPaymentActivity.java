package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class OrderPaymentActivity extends AppCompatActivity {

    RadioButton rb_google, rb_icici, rb_paytm;
    Button btn_next;
    RadioGroup r_group;
    TextView txt_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_payment);

        rb_google = (RadioButton)findViewById(R.id.r_google);
        rb_icici = (RadioButton)findViewById(R.id.r_icici);
        rb_paytm = (RadioButton)findViewById(R.id.r_paytm);
        btn_next = (Button)findViewById(R.id.btn_next);
        r_group = (RadioGroup) findViewById(R.id.r_group);
        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Order payment");

        rb_google.isChecked();
        rb_google.setTextColor(getResources().getColor(R.color.white));
        rb_icici.setTextColor(getResources().getColor(R.color.black));
        rb_paytm.setTextColor(getResources().getColor(R.color.black));

        r_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String value = ((RadioButton)findViewById(r_group.getCheckedRadioButtonId())).getText().toString();
                if (value.equals("Google Pay")){
                    rb_google.setTextColor(getResources().getColor(R.color.white));
                    rb_icici.setTextColor(getResources().getColor(R.color.black));
                    rb_paytm.setTextColor(getResources().getColor(R.color.black));

                    rb_google.setBackgroundResource(R.drawable.change_language_back);
                    rb_icici.setBackgroundResource(R.drawable.change_language_back_white);
                    rb_paytm.setBackgroundResource(R.drawable.change_language_back_white);

                }else if (value.equals("ICICI Bank")){
                    rb_google.setTextColor(getResources().getColor(R.color.black));
                    rb_icici.setTextColor(getResources().getColor(R.color.white));
                    rb_paytm.setTextColor(getResources().getColor(R.color.black));

                    rb_google.setBackgroundResource(R.drawable.change_language_back_white);
                    rb_icici.setBackgroundResource(R.drawable.change_language_back);
                    rb_paytm.setBackgroundResource(R.drawable.change_language_back_white);

                }else if (value.equals("Paytm")){
                    rb_google.setTextColor(getResources().getColor(R.color.black));
                    rb_icici.setTextColor(getResources().getColor(R.color.black));
                    rb_paytm.setTextColor(getResources().getColor(R.color.white));

                    rb_google.setBackgroundResource(R.drawable.change_language_back_white);
                    rb_icici.setBackgroundResource(R.drawable.change_language_back_white);
                    rb_paytm.setBackgroundResource(R.drawable.change_language_back);
                }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile = new Intent(getApplicationContext(), Order_Payment_Detail_Activity.class);
                startActivity(profile);
            }
        });
    }
}