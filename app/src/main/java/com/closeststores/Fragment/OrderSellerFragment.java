package com.closeststores.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.Adapter.CategoryAdapter;
import com.closeststores.Adapter.OrderSubSelleradapter;
import com.closeststores.Helper.RecyclerItemClickListener;
import com.closeststores.OrderDetailSellerActivity;
import com.closeststores.R;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class OrderSellerFragment extends Fragment {

    RecyclerView rv_order_sub;
    OrderSubSelleradapter orderSubAdapter;
    ArrayList<String> orderSub = new ArrayList<>();

    ImageView img_filter;
    NavigationView nav_view;
    DrawerLayout drawer;

    RecyclerView rv_category_list;
    CategoryAdapter categoryAdapter;
    ArrayList<String> categoryCart = new ArrayList<>();

    public static OrderSellerFragment newInstance() {
        OrderSellerFragment fragment = new OrderSellerFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_fragment_order_seller, null);

        rv_order_sub = (RecyclerView) root.findViewById(R.id.rv_sub_order);
        img_filter = (ImageView) root.findViewById(R.id.imgFilter);
        nav_view = (NavigationView) root.findViewById(R.id.nav_view);
        drawer = (DrawerLayout) root.findViewById(R.id.drawer_layout);
        rv_category_list = (RecyclerView)root.findViewById(R.id.rv_category_list);

        orderSub.add("Customer Name");
        orderSub.add("Customer Name");
        orderSub.add("Customer Name");
        orderSub.add("Customer Name");

        categoryCart.add("Vegetables");
        categoryCart.add("Fruits");
        categoryCart.add("Grocery");
        categoryCart.add("HealthCare");
        categoryCart.add("FMCG");

        orderSubAdapter = new OrderSubSelleradapter(getActivity(), orderSub);
        rv_order_sub.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_order_sub.setAdapter(orderSubAdapter);

        categoryAdapter = new CategoryAdapter(getActivity(), categoryCart);
        rv_category_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_category_list.setAdapter(categoryAdapter);

        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });

        rv_order_sub.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent order = new Intent(getActivity(), OrderDetailSellerActivity.class);
                        startActivity(order);
                    }
                })
        );

        return root;
    }

}
