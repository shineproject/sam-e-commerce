package com.closeststores.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.closeststores.Adapter.CustomPagerAdapter;
import com.closeststores.Adapter.ItemAdapter;
import com.closeststores.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class HomeSellerFragment extends Fragment {
    LoopingViewPager viewPager;
    ArrayList<Integer> arrayList;
    RecyclerView rv_list;
    ItemAdapter itemAdapter;
    ArrayList<String> item = new ArrayList<>();

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home_seller, null);
        viewPager = (LoopingViewPager) root.findViewById(R.id.viewpager);
        rv_list = (RecyclerView) root.findViewById(R.id.rv_list);
        arrayList = new ArrayList<>();

        arrayList.add(R.color.colorPrimary);
        arrayList.add(R.color.grey);
        arrayList.add(R.color.colorPrimaryDark);
        arrayList.add(R.color.colorAccent);

        item.add("Man Clothes");
        item.add("Ladies Clothes");
        item.add("Kids Clothes");
        item.add("Man Clothes");

        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getActivity(), arrayList);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) root.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager, true);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }
            @Override
            public void onPageSelected(int i) {
            }
            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        itemAdapter = new ItemAdapter(getActivity(), item);
        rv_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_list.setAdapter(itemAdapter);

        return root;
    }

}
