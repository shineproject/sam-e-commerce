package com.closeststores.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.Adapter.ManageAdapter;
import com.closeststores.AddProductActivity;
import com.closeststores.AddRequestActivity;
import com.closeststores.OrderCartActivity;
import com.closeststores.R;
import com.closeststores.SearchActivity;

import java.util.ArrayList;

public class ManageSellerFragment extends Fragment {

    RecyclerView rv_manage;
    ManageAdapter manageAdapter;
    ArrayList<String> manage = new ArrayList<>();

    RelativeLayout r_search;

    ImageView img_add;

    public static ManageSellerFragment newInstance() {
        ManageSellerFragment fragment = new ManageSellerFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_manage_item, null);

        rv_manage = (RecyclerView) root.findViewById(R.id.rv_manage);
        img_add = (ImageView) root.findViewById(R.id.imgAdd);
        r_search = (RelativeLayout) root.findViewById(R.id.rltSearch);

        manage.add("Man Clothes");
        manage.add("Ladies Clothes");
        manage.add("Kids Clothes");
        manage.add("Perfumes");

        manageAdapter = new ManageAdapter(getActivity(), manage);
        rv_manage.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_manage.setAdapter(manageAdapter);

        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(getActivity(), AddProductActivity.class);
                startActivity(add);
            }
        });

        r_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent add = new Intent(getActivity(), SearchActivity.class);
                startActivity(add);
            }
        });


        return root;

    }
}
