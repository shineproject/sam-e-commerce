package com.closeststores.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.Adapter.CategoryAdapter;
import com.closeststores.Adapter.OrderAdapter;
import com.closeststores.Adapter.OrderSubAdapter;
import com.closeststores.Helper.RecyclerItemClickListener;
import com.closeststores.R;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class OrderFragment extends Fragment {

    RecyclerView rv_order;
    OrderAdapter orderAdapter;
    ArrayList<String> order = new ArrayList<>();

    RecyclerView rv_order_sub;
    OrderSubAdapter orderSubAdapter;
    ArrayList<String> orderSub = new ArrayList<>();

    ImageView img_filter;
    NavigationView nav_view;
    DrawerLayout drawer;

    RecyclerView rv_category_list;
    CategoryAdapter categoryAdapter;
    ArrayList<String> categoryCart = new ArrayList<>();

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_fregment_order, null);

        rv_order = (RecyclerView) root.findViewById(R.id.rv_order);
        rv_order_sub = (RecyclerView) root.findViewById(R.id.rv_sub_order);
        img_filter = (ImageView) root.findViewById(R.id.imgFilter);
        nav_view = (NavigationView) root.findViewById(R.id.nav_view);
        drawer = (DrawerLayout) root.findViewById(R.id.drawer_layout);
        rv_category_list = (RecyclerView)root.findViewById(R.id.rv_category_list);

        order.add("Man Clothes");
        order.add("Ladies Clothes");
        order.add("Kids Clothes");
        order.add("Perfumes");

        orderSub.add("Shope Name");
        orderSub.add("Shope Name");
        orderSub.add("Shope Name");
        orderSub.add("Shope Name");

        categoryCart.add("Vegetables");
        categoryCart.add("Fruits");
        categoryCart.add("Grocery");
        categoryCart.add("HealthCare");
        categoryCart.add("FMCG");

        orderAdapter = new OrderAdapter(getActivity(), order);
        rv_order.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rv_order.setAdapter(orderAdapter);

        orderSubAdapter = new OrderSubAdapter(getActivity(), orderSub);
        rv_order_sub.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_order_sub.setAdapter(orderSubAdapter);

        categoryAdapter = new CategoryAdapter(getActivity(), categoryCart);
        rv_category_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_category_list.setAdapter(categoryAdapter);

        rv_order.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        rv_order.setVisibility(View.GONE);
                        rv_order_sub.setVisibility(View.VISIBLE);
                    }
                })
        );

        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });

        root.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    if (rv_order.getVisibility() == View.VISIBLE){
                        getActivity().onBackPressed();
                    }else {
                        rv_order_sub.setVisibility(View.GONE);
                        rv_order.setVisibility(View.VISIBLE);
                    }
                    return true;
                }
                return false;
            }

        });

        return root;
    }

}

