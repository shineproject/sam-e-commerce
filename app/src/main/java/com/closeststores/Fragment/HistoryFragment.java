package com.closeststores.Fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.closeststores.Adapter.CompletedAdapter;
import com.closeststores.Adapter.CurrentAdapter;
import com.closeststores.Adapter.HistoryAdapter;
import com.closeststores.Helper.RecyclerItemClickListener;
import com.closeststores.OrderDetailActivity;
import com.closeststores.R;

import java.util.ArrayList;

public class HistoryFragment extends Fragment {

    RecyclerView rv_history;
    HistoryAdapter historyAdapter;
    ArrayList<String> history = new ArrayList<>();

    RecyclerView rv_current;
    CurrentAdapter currentAdapter;
    ArrayList<String> current = new ArrayList<>();

    RecyclerView rv_completed;
    CompletedAdapter completeAdapter;
    ArrayList<String> complete = new ArrayList<>();

    TextView txt_current, txt_completed;
    View v_current, v_completed;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_history, null);

        rv_history = (RecyclerView) root.findViewById(R.id.rv_history);
        rv_current = (RecyclerView) root.findViewById(R.id.rv_current);
        rv_completed = (RecyclerView) root.findViewById(R.id.rv_completed);
        txt_current = (TextView) root.findViewById(R.id.txt_current_order);
        txt_completed = (TextView) root.findViewById(R.id.txt_completed_order);
        v_current = (View) root.findViewById(R.id.line_current);
        v_completed = (View) root.findViewById(R.id.line_completed);

        txt_current.setTextColor(getResources().getColor(R.color.colorPrimary));
        txt_completed.setTextColor(getResources().getColor(R.color.grey));
        v_current.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        v_completed.setBackgroundColor(getResources().getColor(R.color.grey));

        txt_current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv_current.setVisibility(View.VISIBLE);
                rv_completed.setVisibility(View.GONE);

                txt_current.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_completed.setTextColor(getResources().getColor(R.color.grey));
                v_current.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                v_completed.setBackgroundColor(getResources().getColor(R.color.grey));
            }
        });

        txt_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv_current.setVisibility(View.GONE);
                rv_completed.setVisibility(View.VISIBLE);

                txt_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_current.setTextColor(getResources().getColor(R.color.grey));
                v_completed.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                v_current.setBackgroundColor(getResources().getColor(R.color.grey));
            }
        });

        history.add("Seller Name");
        history.add("Seller Name");
        history.add("Seller Name");
        history.add("Seller Name");
        history.add("Seller Name");

        current.add("Man Clothes");
        current.add("Ladies Clothes");
        current.add("Kids Clothes");
        current.add("Perfumes");

        complete.add("Man Clothes");
        complete.add("Ladies Clothes");
        complete.add("Kids Clothes");
        complete.add("Perfumes");

        historyAdapter = new HistoryAdapter(getActivity(), history);
        rv_history.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_history.setAdapter(historyAdapter);

        currentAdapter = new CurrentAdapter(getActivity(), current);
        rv_current.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_current.setAdapter(currentAdapter);

        completeAdapter = new CompletedAdapter(getActivity(), complete);
        rv_completed.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayout.VERTICAL, false));
        rv_completed.setAdapter(completeAdapter);

        rv_current.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent order = new Intent(getActivity(), OrderDetailActivity.class);
                        startActivity(order);
                    }
                })
        );

        return root;
    }
}
