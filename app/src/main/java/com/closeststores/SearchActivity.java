package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.closeststores.Adapter.OrderCartAdapter;
import com.closeststores.Adapter.SearchAdapter;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    RecyclerView rv_search;
    SearchAdapter searchAdapter;
    ArrayList<String> search = new ArrayList<>();

    EditText et_search;
    ImageView img_close;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        rv_search = (RecyclerView) findViewById(R.id.rv_search_list);
        et_search = (EditText) findViewById(R.id.search);
        img_close = (ImageView) findViewById(R.id.img_close);

        search.add("Man Clothes");
        search.add("Ladies Clothes");
        search.add("Kids Clothes");
        search.add("Perfumes");

        searchAdapter = new SearchAdapter(getApplicationContext(), search);
        rv_search.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        rv_search.setAdapter(searchAdapter);

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    searchAdapter.filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
}