package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.closeststores.Adapter.OrderCartAdapter;
import com.closeststores.Adapter.WishListAdapter;

import java.util.ArrayList;

public class WishListActivity extends AppCompatActivity {

    RecyclerView rv_wish_list;
    WishListAdapter wishListAdapter;
    ArrayList<String> wishList = new ArrayList<>();

    TextView txt_name;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        rv_wish_list = (RecyclerView) findViewById(R.id.rv_wishlist);
        txt_name = (TextView) findViewById(R.id.txtTitle);

        txt_name.setText("Wish List");

        wishList.add("Man Clothes");
        wishList.add("Ladies Clothes");
        wishList.add("Kids Clothes");
        wishList.add("Perfumes");

        wishListAdapter = new WishListAdapter(getApplicationContext(), wishList);
        rv_wish_list.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        rv_wish_list.setAdapter(wishListAdapter);
    }
}