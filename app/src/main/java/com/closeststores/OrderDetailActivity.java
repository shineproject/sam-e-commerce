package com.closeststores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import params.com.stepview.StatusViewScroller;

public class OrderDetailActivity extends AppCompatActivity {

    TextView txt_name;
    StatusViewScroller step;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        txt_name = (TextView) findViewById(R.id.txtTitle);
        step = findViewById(R.id.stepview);

        txt_name.setText("Order Detail");
        step.getStatusView().setCurrentCount(5);
    }
}
